; Function Attrs: nounwind ssp uwtable
define %struct.String* @Object_type_name(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  %tmp = alloca %struct.String*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = call i8* @malloc(i64 40)
  %3 = bitcast i8* %2 to %struct.String*
  store %struct.String* %3, %struct.String** %tmp, align 8
  %4 = load %struct.String** %tmp, align 8
  %5 = getelementptr inbounds %struct.String* %4, i32 0, i32 1
  store i32 40, i32* %5, align 4
  %6 = load %struct.String** %tmp, align 8
  %7 = getelementptr inbounds %struct.String* %6, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @String_name, i32 0, i32 0), i8** %7, align 8
  %8 = load i8** %1, align 8
  %9 = bitcast i8* %8 to %struct.Object*
  %10 = getelementptr inbounds %struct.Object* %9, i32 0, i32 2
  %11 = load i8** %10, align 8
  %12 = load %struct.String** %tmp, align 8
  %13 = getelementptr inbounds %struct.String* %12, i32 0, i32 3
  store i8* %11, i8** %13, align 8
  %14 = load %struct.String** %tmp, align 8
  %15 = getelementptr inbounds %struct.String* %14, i32 0, i32 3
  %16 = load i8** %15, align 8
  %17 = call i64 @strlen(i8* %16)
  %18 = trunc i64 %17 to i32
  %19 = load %struct.String** %tmp, align 8
  %20 = getelementptr inbounds %struct.String* %19, i32 0, i32 4
  store i32 %18, i32* %20, align 4
  %21 = load %struct.String** %tmp, align 8
  ret %struct.String* %21
}

declare i8* @malloc(i64) #1

declare i64 @strlen(i8*) #1

; Function Attrs: nounwind ssp uwtable
define %struct.Object* @Object_abort(i8* %self_obj) #0 {
  %1 = alloca %struct.Object*, align 8
  %2 = alloca i8*, align 8
  store i8* %self_obj, i8** %2, align 8
  %3 = load i8** %2, align 8
  %4 = bitcast i8* %3 to %struct.Object*
  %5 = getelementptr inbounds %struct.Object* %4, i32 0, i32 2
  %6 = load i8** %5, align 8
  %7 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([22 x i8]* @.str, i32 0, i32 0), i8* %6)
  call void @exit(i32 0) #5
  unreachable
                                                  ; No predecessors!
  %9 = load %struct.Object** %1
  ret %struct.Object* %9
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

; Function Attrs: nounwind ssp uwtable
define %struct.IO* @IO_out_string(i8* %self_obj, %struct.String* byval align 8 %str) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = getelementptr inbounds %struct.String* %str, i32 0, i32 3
  %3 = load i8** %2, align 8
  %4 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @str_str, i32 0, i32 0), i8* %3)
  %5 = load i8** %1, align 8
  %6 = bitcast i8* %5 to %struct.IO*
  ret %struct.IO* %6
}

; Function Attrs: nounwind ssp uwtable
define %struct.IO* @IO_out_int(i8* %self_obj, %struct.Int* byval align 8 %val) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = getelementptr inbounds %struct.Int* %val, i32 0, i32 3
  %3 = load i32* %2, align 4
  %4 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @int_str, i32 0, i32 0), i32 %3)
  %5 = load i8** %1, align 8
  %6 = bitcast i8* %5 to %struct.IO*
  ret %struct.IO* %6
}

; Function Attrs: nounwind ssp uwtable
define %struct.String* @IO_in_string(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  %temp = alloca %struct.String*, align 8
  %tmp_str = alloca i8*, align 8
  %str_len = alloca i32, align 4
  store i8* %self_obj, i8** %1, align 8
  %2 = call i8* @malloc(i64 40)
  %3 = bitcast i8* %2 to %struct.String*
  store %struct.String* %3, %struct.String** %temp, align 8
  %4 = call i8* @malloc(i64 1025)
  store i8* %4, i8** %tmp_str, align 8
  store i32 0, i32* %str_len, align 4
  %5 = load i8** %tmp_str, align 8
  %6 = call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds ([3 x i8]* @str_str, i32 0, i32 0), i8* %5)
  %7 = load i8** %tmp_str, align 8
  %8 = call i64 @strlen(i8* %7)
  %9 = trunc i64 %8 to i32
  store i32 %9, i32* %str_len, align 4
  %10 = load i32* %str_len, align 4
  %11 = load %struct.String** %temp, align 8
  %12 = getelementptr inbounds %struct.String* %11, i32 0, i32 4
  store i32 %10, i32* %12, align 4
  %13 = load %struct.String** %temp, align 8
  %14 = getelementptr inbounds %struct.String* %13, i32 0, i32 1
  store i32 40, i32* %14, align 4
  %15 = load i8** %tmp_str, align 8
  %16 = load %struct.String** %temp, align 8
  %17 = getelementptr inbounds %struct.String* %16, i32 0, i32 3
  store i8* %15, i8** %17, align 8
  %18 = load %struct.String** %temp, align 8
  %19 = getelementptr inbounds %struct.String* %18, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @String_name, i32 0, i32 0), i8** %19, align 8
  %20 = load %struct.String** %temp, align 8
  ret %struct.String* %20
}

declare i32 @scanf(i8*, ...) #1

; Function Attrs: nounwind ssp uwtable
define %struct.Int* @IO_in_int(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  %temp = alloca %struct.Int*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = call i8* @malloc(i64 32)
  %3 = bitcast i8* %2 to %struct.Int*
  store %struct.Int* %3, %struct.Int** %temp, align 8
  %4 = load %struct.Int** %temp, align 8
  %5 = getelementptr inbounds %struct.Int* %4, i32 0, i32 3
  %6 = call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds ([3 x i8]* @int_str, i32 0, i32 0), i32* %5)
  %7 = load %struct.Int** %temp, align 8
  %8 = getelementptr inbounds %struct.Int* %7, i32 0, i32 1
  store i32 32, i32* %8, align 4
  %9 = load %struct.Int** %temp, align 8
  %10 = getelementptr inbounds %struct.Int* %9, i32 0, i32 2
  store i8* getelementptr inbounds ([4 x i8]* @Int_name, i32 0, i32 0), i8** %10, align 8
  %11 = load %struct.Int** %temp, align 8
  ret %struct.Int* %11
}

; Function Attrs: nounwind ssp uwtable
define %struct.Object* @Object_copy(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  %copy = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = bitcast i8* %2 to %struct.Object*
  %4 = getelementptr inbounds %struct.Object* %3, i32 0, i32 1
  %5 = load i32* %4, align 4
  %6 = sext i32 %5 to i64
  %7 = call i8* @malloc(i64 %6)
  store i8* %7, i8** %copy, align 8
  %8 = load i8** %copy, align 8
  %9 = bitcast i8* %8 to %struct.Object*
  ret %struct.Object* %9
}

; Function Attrs: nounwind ssp uwtable
define %struct.String* @String_concat(i8* %self_obj, %struct.String* byval align 8 %str) #0 {
  %1 = alloca i8*, align 8
  %temp_self_obj = alloca %struct.String*, align 8
  %temp_str = alloca %struct.String*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = bitcast i8* %2 to %struct.String*
  store %struct.String* %3, %struct.String** %temp_self_obj, align 8
  %4 = call i8* @malloc(i64 40)
  %5 = bitcast i8* %4 to %struct.String*
  store %struct.String* %5, %struct.String** %temp_str, align 8
  %6 = load %struct.String** %temp_str, align 8
  %7 = getelementptr inbounds %struct.String* %6, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @String_name, i32 0, i32 0), i8** %7, align 8
  %8 = load %struct.String** %temp_str, align 8
  %9 = getelementptr inbounds %struct.String* %8, i32 0, i32 1
  store i32 40, i32* %9, align 4
  %10 = load %struct.String** %temp_self_obj, align 8
  %11 = getelementptr inbounds %struct.String* %10, i32 0, i32 3
  %12 = load i8** %11, align 8
  %13 = call i64 @strlen(i8* %12)
  %14 = getelementptr inbounds %struct.String* %str, i32 0, i32 3
  %15 = load i8** %14, align 8
  %16 = call i64 @strlen(i8* %15)
  %17 = add i64 %13, %16
  %18 = mul i64 1, %17
  %19 = call i8* @malloc(i64 %18)
  %20 = load %struct.String** %temp_str, align 8
  %21 = getelementptr inbounds %struct.String* %20, i32 0, i32 3
  store i8* %19, i8** %21, align 8
  %22 = load %struct.String** %temp_str, align 8
  %23 = getelementptr inbounds %struct.String* %22, i32 0, i32 3
  %24 = load i8** %23, align 8
  %25 = load %struct.String** %temp_self_obj, align 8
  %26 = getelementptr inbounds %struct.String* %25, i32 0, i32 3
  %27 = load i8** %26, align 8
  %28 = load %struct.String** %temp_str, align 8
  %29 = getelementptr inbounds %struct.String* %28, i32 0, i32 3
  %30 = load i8** %29, align 8
  %31 = call i64 @llvm.objectsize.i64.p0i8(i8* %30, i1 false)
  %32 = call i8* @__strcpy_chk(i8* %24, i8* %27, i64 %31) #6
  %33 = load %struct.String** %temp_str, align 8
  %34 = getelementptr inbounds %struct.String* %33, i32 0, i32 3
  %35 = load i8** %34, align 8
  %36 = getelementptr inbounds %struct.String* %str, i32 0, i32 3
  %37 = load i8** %36, align 8
  %38 = load %struct.String** %temp_str, align 8
  %39 = getelementptr inbounds %struct.String* %38, i32 0, i32 3
  %40 = load i8** %39, align 8
  %41 = call i64 @llvm.objectsize.i64.p0i8(i8* %40, i1 false)
  %42 = call i8* @__strcat_chk(i8* %35, i8* %37, i64 %41) #6
  %43 = load %struct.String** %temp_str, align 8
  %44 = getelementptr inbounds %struct.String* %43, i32 0, i32 3
  %45 = load i8** %44, align 8
  %46 = call i64 @strlen(i8* %45)
  %47 = trunc i64 %46 to i32
  %48 = load %struct.String** %temp_str, align 8
  %49 = getelementptr inbounds %struct.String* %48, i32 0, i32 4
  store i32 %47, i32* %49, align 4
  %50 = load %struct.String** %temp_str, align 8
  ret %struct.String* %50
}

; Function Attrs: nounwind
declare i8* @__strcpy_chk(i8*, i8*, i64) #3

; Function Attrs: nounwind readnone
declare i64 @llvm.objectsize.i64.p0i8(i8*, i1) #4

; Function Attrs: nounwind
declare i8* @__strcat_chk(i8*, i8*, i64) #3

; Function Attrs: nounwind ssp uwtable
define %struct.Int* @String_length(i8* %self_obj) #0 {
%1 = alloca i8*, align 8
%temp_self_obj = alloca %struct.String*, align 8
%temp_str = alloca %struct.Int*, align 8
store i8* %self_obj, i8** %1, align 8
%2 = load i8** %1, align 8
%3 = bitcast i8* %2 to %struct.String*
store %struct.String* %3, %struct.String** %temp_self_obj, align 8
%4 = call i8* @malloc(i64 32)
%5 = bitcast i8* %4 to %struct.Int*
store %struct.Int* %5, %struct.Int** %temp_str, align 8
%6 = load %struct.Int** %temp_str, align 8
%7 = getelementptr inbounds %struct.Int* %6, i32 0, i32 2
store i8* getelementptr inbounds ([4 x i8]* @Int_name, i32 0, i32 0), i8** %7, align 8
%8 = load %struct.Int** %temp_str, align 8
%9 = getelementptr inbounds %struct.Int* %8, i32 0, i32 1
store i32 32, i32* %9, align 4
%10 = load %struct.String** %temp_self_obj, align 8
%11 = getelementptr inbounds %struct.String* %10, i32 0, i32 3
%12 = load i8** %11, align 8
%13 = call i64 @strlen(i8* %12)
%14 = trunc i64 %13 to i32
%15 = load %struct.Int** %temp_str, align 8
%16 = getelementptr inbounds %struct.Int* %15, i32 0, i32 3
store i32 %14, i32* %16, align 4
%17 = load %struct.Int** %temp_str, align 8
ret %struct.Int* %17
}

; Function Attrs: nounwind ssp uwtable
define %struct.String* @String_substr(i8* %self_obj, %struct.Int* %index1, %struct.Int* %index2) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca %struct.Int*, align 8
  %3 = alloca %struct.Int*, align 8
  %temp_self_obj = alloca %struct.String*, align 8
  %temp_str = alloca %struct.String*, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i8* %self_obj, i8** %1, align 8
  store %struct.Int* %index1, %struct.Int** %2, align 8
  store %struct.Int* %index2, %struct.Int** %3, align 8
  %4 = load i8** %1, align 8
  %5 = bitcast i8* %4 to %struct.String*
  store %struct.String* %5, %struct.String** %temp_self_obj, align 8
  %6 = call i8* @malloc(i64 40)
  %7 = bitcast i8* %6 to %struct.String*
  store %struct.String* %7, %struct.String** %temp_str, align 8
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  %8 = load %struct.String** %temp_str, align 8
  %9 = getelementptr inbounds %struct.String* %8, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @String_name, i32 0, i32 0), i8** %9, align 8
  %10 = load %struct.String** %temp_str, align 8
  %11 = getelementptr inbounds %struct.String* %10, i32 0, i32 1
  store i32 40, i32* %11, align 4
  %12 = load %struct.Int** %3, align 8
  %13 = getelementptr inbounds %struct.Int* %12, i32 0, i32 3
  %14 = load i32* %13, align 4
  %15 = add nsw i32 %14, 1
  %16 = sext i32 %15 to i64
  %17 = mul i64 1, %16
  %18 = call i8* @malloc(i64 %17)
  %19 = load %struct.String** %temp_str, align 8
  %20 = getelementptr inbounds %struct.String* %19, i32 0, i32 3
  store i8* %18, i8** %20, align 8
  %21 = load %struct.Int** %2, align 8
  %22 = getelementptr inbounds %struct.Int* %21, i32 0, i32 3
  %23 = load i32* %22, align 4
  store i32 %23, i32* %i, align 4
  store i32 0, i32* %j, align 4
  br label %24

; <label>:24                                      ; preds = %44, %0
  %25 = load i32* %j, align 4
  %26 = load %struct.Int** %3, align 8
  %27 = getelementptr inbounds %struct.Int* %26, i32 0, i32 3
  %28 = load i32* %27, align 4
  %29 = icmp slt i32 %25, %28
  br i1 %29, label %30, label %49

; <label>:30                                      ; preds = %24
  %31 = load i32* %i, align 4
  %32 = sext i32 %31 to i64
  %33 = load %struct.String** %temp_self_obj, align 8
  %34 = getelementptr inbounds %struct.String* %33, i32 0, i32 3
  %35 = load i8** %34, align 8
  %36 = getelementptr inbounds i8* %35, i64 %32
  %37 = load i8* %36, align 1
  %38 = load i32* %j, align 4
  %39 = sext i32 %38 to i64
  %40 = load %struct.String** %temp_str, align 8
  %41 = getelementptr inbounds %struct.String* %40, i32 0, i32 3
  %42 = load i8** %41, align 8
  %43 = getelementptr inbounds i8* %42, i64 %39
  store i8 %37, i8* %43, align 1
  br label %44

; <label>:44                                      ; preds = %30
  %45 = load i32* %i, align 4
  %46 = add nsw i32 %45, 1
  store i32 %46, i32* %i, align 4
  %47 = load i32* %j, align 4
  %48 = add nsw i32 %47, 1
  store i32 %48, i32* %j, align 4
  br label %24

; <label>:49                                      ; preds = %24
  %50 = load i32* %j, align 4
  %51 = sext i32 %50 to i64
  %52 = load %struct.String** %temp_str, align 8
  %53 = getelementptr inbounds %struct.String* %52, i32 0, i32 3
  %54 = load i8** %53, align 8
  %55 = getelementptr inbounds i8* %54, i64 %51
  store i8 0, i8* %55, align 1
  %56 = load %struct.String** %temp_str, align 8
  %57 = getelementptr inbounds %struct.String* %56, i32 0, i32 3
  %58 = load i8** %57, align 8
  %59 = call i64 @strlen(i8* %58)
  %60 = trunc i64 %59 to i32
  %61 = load %struct.String** %temp_str, align 8
  %62 = getelementptr inbounds %struct.String* %61, i32 0, i32 4
  store i32 %60, i32* %62, align 4
  %63 = load %struct.String** %temp_str, align 8
  ret %struct.String* %63
}




; Function Attrs: nounwind ssp uwtable
define %struct.Int* @Int_copy(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call %struct.Object* @Object_copy(i8* %2)
  %4 = bitcast %struct.Object* %3 to %struct.Int*
  ret %struct.Int* %4
}

; Function Attrs: nounwind ssp uwtable
define %struct.String* @String_copy(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call %struct.Object* @Object_copy(i8* %2)
  %4 = bitcast %struct.Object* %3 to %struct.String*
  ret %struct.String* %4
}

; Function Attrs: nounwind ssp uwtable
define %struct.Bool* @Bool_copy(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call %struct.Object* @Object_copy(i8* %2)
  %4 = bitcast %struct.Object* %3 to %struct.Bool*
  ret %struct.Bool* %4
}

; Function Attrs: nounwind ssp uwtable
define %struct.IO* @IO_copy(i8* %self_obj) #0 {
  %1 = alloca i8*, align 8
  store i8* %self_obj, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call %struct.Object* @Object_copy(i8* %2)
  %4 = bitcast %struct.Object* %3 to %struct.IO*
  ret %struct.IO* %4
}

; Function Attrs: nounwind ssp uwtable
define %struct.Object* @.Object() #0 {
  %temp = alloca %struct.Object*, align 8
  %1 = call i8* @malloc(i64 24)
  %2 = bitcast i8* %1 to %struct.Object*
  store %struct.Object* %2, %struct.Object** %temp, align 8
  %3 = load %struct.Object** %temp, align 8
  %4 = getelementptr inbounds %struct.Object* %3, i32 0, i32 1
  store i32 24, i32* %4, align 4
  %5 = load %struct.Object** %temp, align 8
  %6 = getelementptr inbounds %struct.Object* %5, i32 0, i32 0
  store %struct.Object_functions* @Object_functions_proto, %struct.Object_functions** %6, align 8
  %7 = load %struct.Object** %temp, align 8
  %8 = getelementptr inbounds %struct.Object* %7, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @Object_name, i32 0, i32 0), i8** %8, align 8
  %9 = load %struct.Object** %temp, align 8
  ret %struct.Object* %9
}

; Function Attrs: nounwind ssp uwtable
define %struct.Int* @.Int() #0 {
  %temp = alloca %struct.Int*, align 8
  %1 = call i8* @malloc(i64 32)
  %2 = bitcast i8* %1 to %struct.Int*
  store %struct.Int* %2, %struct.Int** %temp, align 8
  %3 = load %struct.Int** %temp, align 8
  %4 = getelementptr inbounds %struct.Int* %3, i32 0, i32 1
  store i32 32, i32* %4, align 4
  %5 = load %struct.Int** %temp, align 8
  %6 = getelementptr inbounds %struct.Int* %5, i32 0, i32 0
  store %struct.Object_functions* bitcast (%struct.Int_functions* @Int_functions_proto to %struct.Object_functions*), %struct.Object_functions** %6, align 8
  %7 = load %struct.Int** %temp, align 8
  %8 = getelementptr inbounds %struct.Int* %7, i32 0, i32 2
  store i8* getelementptr inbounds ([4 x i8]* @Int_name, i32 0, i32 0), i8** %8, align 8
  %9 = load %struct.Int** %temp, align 8
  %10 = getelementptr inbounds %struct.Int* %9, i32 0, i32 3
  store i32 0, i32* %10, align 4
  %11 = load %struct.Int** %temp, align 8
  ret %struct.Int* %11
}

; Function Attrs: nounwind ssp uwtable
define %struct.String* @.String() #0 {
  %temp = alloca %struct.String*, align 8
  %1 = call i8* @malloc(i64 40)
  %2 = bitcast i8* %1 to %struct.String*
  store %struct.String* %2, %struct.String** %temp, align 8
  %3 = load %struct.String** %temp, align 8
  %4 = getelementptr inbounds %struct.String* %3, i32 0, i32 1
  store i32 40, i32* %4, align 4
  %5 = load %struct.String** %temp, align 8
  %6 = getelementptr inbounds %struct.String* %5, i32 0, i32 0
  store %struct.Object_functions* bitcast (%struct.String_functions* @String_functions_proto to %struct.Object_functions*), %struct.Object_functions** %6, align 8
  %7 = load %struct.String** %temp, align 8
  %8 = getelementptr inbounds %struct.String* %7, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @String_name, i32 0, i32 0), i8** %8, align 8
  %9 = load %struct.String** %temp, align 8
  %10 = getelementptr inbounds %struct.String* %9, i32 0, i32 3
  store i8* getelementptr inbounds ([1 x i8]* @null_str, i32 0, i32 0), i8** %10, align 8
  %11 = load %struct.String** %temp, align 8
  %12 = getelementptr inbounds %struct.String* %11, i32 0, i32 4
  store i32 0, i32* %12, align 4
  %13 = load %struct.String** %temp, align 8
  ret %struct.String* %13
}

; Function Attrs: nounwind ssp uwtable
define %struct.Bool* @.Bool() #0 {
  %temp = alloca %struct.Bool*, align 8
  %1 = call i8* @malloc(i64 32)
  %2 = bitcast i8* %1 to %struct.Bool*
  store %struct.Bool* %2, %struct.Bool** %temp, align 8
  %3 = load %struct.Bool** %temp, align 8
  %4 = getelementptr inbounds %struct.Bool* %3, i32 0, i32 1
  store i32 32, i32* %4, align 4
  %5 = load %struct.Bool** %temp, align 8
  %6 = getelementptr inbounds %struct.Bool* %5, i32 0, i32 0
  store %struct.Object_functions* bitcast (%struct.Bool_functions* @Bool_functions_proto to %struct.Object_functions*), %struct.Object_functions** %6, align 8
  %7 = load %struct.Bool** %temp, align 8
  %8 = getelementptr inbounds %struct.Bool* %7, i32 0, i32 2
  store i8* getelementptr inbounds ([5 x i8]* @Bool_name, i32 0, i32 0), i8** %8, align 8
  %9 = load %struct.Bool** %temp, align 8
  %10 = getelementptr inbounds %struct.Bool* %9, i32 0, i32 3
  store i32 0, i32* %10, align 4
  %11 = load %struct.Bool** %temp, align 8
  ret %struct.Bool* %11
}

; Function Attrs: nounwind ssp uwtable
define %struct.IO* @.IO() #0 {
  %temp = alloca %struct.IO*, align 8
  %1 = call i8* @malloc(i64 24)
  %2 = bitcast i8* %1 to %struct.IO*
  store %struct.IO* %2, %struct.IO** %temp, align 8
  %3 = load %struct.IO** %temp, align 8
  %4 = getelementptr inbounds %struct.IO* %3, i32 0, i32 1
  store i32 24, i32* %4, align 4
  %5 = load %struct.IO** %temp, align 8
  %6 = getelementptr inbounds %struct.IO* %5, i32 0, i32 0
  store %struct.Object_functions* bitcast (%struct.IO_functions* @IO_functions_proto to %struct.Object_functions*), %struct.Object_functions** %6, align 8
  %7 = load %struct.IO** %temp, align 8
  %8 = getelementptr inbounds %struct.IO* %7, i32 0, i32 2
  store i8* getelementptr inbounds ([3 x i8]* @IO_name, i32 0, i32 0), i8** %8, align 8
  %9 = load %struct.IO** %temp, align 8
  ret %struct.IO* %9
}

declare i32 @strcmp(i8*, i8*) #2

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone }
attributes #5 = { noreturn }
attributes #6 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.0 (clang-600.0.54) (based on LLVM 3.5svn)"}
