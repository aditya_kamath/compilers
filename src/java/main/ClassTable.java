package main;

/*
 Copyright (c) 2000 The Regents of the University of California.
 All rights reserved.

 Permission to use, copy, modify, and distribute this software for any
 purpose, without fee, and without written agreement is hereby granted,
 provided that the above copyright notice and the following two
 paragraphs appear in all copies of this software.

 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
 PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

// This is a project skeleton file

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.sun.javafx.binding.SelectBinding.AsBoolean;

/**
 * This class may be used to contain the semantic information such as the
 * inheritance graph. You may use it or not as you like: it is only here to
 * provide a container for the supplied methods.
 */
public class ClassTable {
	private int semantErrors;
	private PrintStream errorStream;
	public HashSet<AbstractSymbol> classListSet;
	public ArrayList<AbstractSymbol> classList;
	public HashMap<AbstractSymbol, Class_> SymToClassMap;
	public class_c Object_class, IO_class, Int_class, Bool_class, Str_class;
	int adjMat[][];
	boolean foundMain;

	/**
	 * Creates data structures representing basic Cool classes (Object, IO, Int,
	 * Bool, String). Please note: as is this method does not do anything
	 * useful; you will need to edit it to make if do what you want.
	 * */
	private void installBasicClasses() {
		AbstractSymbol filename = AbstractTable.stringtable
				.addString("<basic class>");

		// The following demonstrates how to create dummy parse trees to
		// refer to basic Cool classes. There's no need for method
		// bodies -- these are already built into the runtime system.

		// IMPORTANT: The results of the following expressions are
		// stored in local variables. You will want to do something
		// with those variables at the end of this method to make this
		// code meaningful.

		// The Object class has no parent class. Its methods are
		// cool_abort() : Object aborts the program
		// type_name() : Str returns a string representation
		// of class name
		// copy() : SELF_TYPE returns a copy of the object

		Object_class = new class_c(
				0,
				TreeConstants.Object_,
				TreeConstants.No_class,
				new Features(0)
						.appendElement(
								new method(0, TreeConstants.cool_abort,
										new Formals(0), TreeConstants.Object_,
										new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.type_name,
										new Formals(0), TreeConstants.Str,
										new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.copy,
										new Formals(0),
										TreeConstants.SELF_TYPE, new no_expr(0))),
				filename);

		// The IO class inherits from Object. Its methods are
		// out_string(Str) : SELF_TYPE writes a string to the output
		// out_int(Int) : SELF_TYPE "    an int    " "     "
		// in_string() : Str reads a string from the input
		// in_int() : Int "   an int     " "     "

		IO_class = new class_c(
				0,
				TreeConstants.IO,
				TreeConstants.Object_,
				new Features(0)
						.appendElement(
								new method(0, TreeConstants.out_string,
										new Formals(0)
												.appendElement(new formalc(0,
														TreeConstants.arg,
														TreeConstants.Str)),
										TreeConstants.SELF_TYPE, new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.out_int,
										new Formals(0)
												.appendElement(new formalc(0,
														TreeConstants.arg,
														TreeConstants.Int)),
										TreeConstants.SELF_TYPE, new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.in_string,
										new Formals(0), TreeConstants.Str,
										new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.in_int,
										new Formals(0), TreeConstants.Int,
										new no_expr(0))), filename);

		// The Int class has no methods and only a single attribute, the
		// "val" for the integer.

		Int_class = new class_c(0, TreeConstants.Int, TreeConstants.Object_,
				new Features(0).appendElement(new attr(0, TreeConstants.val,
						TreeConstants.prim_slot, new no_expr(0))), filename);

		// Bool also has only the "val" slot.
		Bool_class = new class_c(0, TreeConstants.Bool, TreeConstants.Object_,
				new Features(0).appendElement(new attr(0, TreeConstants.val,
						TreeConstants.prim_slot, new no_expr(0))), filename);

		// The class Str has a number of slots and operations:
		// val the length of the string
		// str_field the string itself
		// length() : Int returns length of the string
		// concat(arg: Str) : Str performs string concatenation
		// substr(arg: Int, arg2: Int): Str substring selection

		Str_class = new class_c(
				0,
				TreeConstants.Str,
				TreeConstants.Object_,
				new Features(0)
						.appendElement(
								new attr(0, TreeConstants.val,
										TreeConstants.Int, new no_expr(0)))
						.appendElement(
								new attr(0, TreeConstants.str_field,
										TreeConstants.prim_slot, new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.length,
										new Formals(0), TreeConstants.Int,
										new no_expr(0)))
						.appendElement(
								new method(0, TreeConstants.concat,
										new Formals(0)
												.appendElement(new formalc(0,
														TreeConstants.arg,
														TreeConstants.Str)),
										TreeConstants.Str, new no_expr(0)))
						.appendElement(
								new method(
										0,
										TreeConstants.substr,
										new Formals(0)
												.appendElement(
														new formalc(
																0,
																TreeConstants.arg,
																TreeConstants.Int))
												.appendElement(
														new formalc(
																0,
																TreeConstants.arg2,
																TreeConstants.Int)),
										TreeConstants.Str, new no_expr(0))),
				filename);

		/*
		 * Do somethind with Object_class, IO_class, Int_class, Bool_class, and
		 * Str_class here
		 */

		// NOT TO BE INCLUDED IN SKELETON
		// put basic classes in HashMap
		SymToClassMap.put(TreeConstants.Object_, Object_class);
		SymToClassMap.put(TreeConstants.IO, IO_class);
		SymToClassMap.put(TreeConstants.Str, Str_class);
		SymToClassMap.put(TreeConstants.Int, Int_class);
		SymToClassMap.put(TreeConstants.Bool, Bool_class);
		/*
		 * Object_class.dump_with_types(System.err, 0);
		 * IO_class.dump_with_types(System.err, 0);
		 * Int_class.dump_with_types(System.err, 0);
		 * Bool_class.dump_with_types(System.err, 0);
		 * Str_class.dump_with_types(System.err, 0);
		 */
	}

	public ArrayList<Class_> getBaiscClasses() {
		ArrayList<Class_> basicClassList = new ArrayList<>();
		basicClassList.add(Object_class);
		basicClassList.add(IO_class);
		basicClassList.add(Str_class);
		basicClassList.add(Int_class);
		basicClassList.add(Bool_class);
		return basicClassList;

	}

	public ClassTable(Classes cls) {
		semantErrors = 0;
		errorStream = System.err;
		classListSet = new HashSet();
		classList = new ArrayList<>();
		SymToClassMap = new HashMap<>();
		installBasicClasses();
		buildVertices(cls);
		// TODO : Build Graph and check for cycles

		/* fill this in */
	}

	/**
	 * Prints line number and file name of the given class.
	 * 
	 * Also increments semantic error count.
	 * 
	 * @param c
	 *            the class
	 * @return a print stream to which the rest of the error message is to be
	 *         printed.
	 * 
	 * */
	public PrintStream semantError(class_c c) {
		return semantError(c.getFilename(), c);
	}

	/**
	 * Prints the file name and the line number of the given tree node.
	 * 
	 * Also increments semantic error count.
	 * 
	 * @param filename
	 *            the file name
	 * @param t
	 *            the tree node
	 * @return a print stream to which the rest of the error message is to be
	 *         printed.
	 * 
	 * */
	public PrintStream semantError(AbstractSymbol filename, TreeNode t) {
		errorStream.print(filename + ":" + t.getLineNumber() + ": ");
		return semantError();
	}

	/**
	 * Increments semantic error count and returns the print stream for error
	 * messages.
	 * 
	 * @return a print stream to which the error message is to be printed.
	 * 
	 * */
	public PrintStream semantError() {
		semantErrors++;
		return errorStream;
	}

	/** Returns true if there are any static semantic errors. */
	public boolean errors() {
		return semantErrors != 0;
	}

	// NOT TO BE INCLUDED IN SKELETON
	/*public static void main(String[] args) {
		new ClassTable(null).installBasicClasses();
	}*/

	public void buildVertices(Classes cls) {
		ArrayList<AbstractSymbol> classList = new ArrayList<>();
		foundMain = false;

		// add basic classes
		classList.add(TreeConstants.Object_);
		classList.add(TreeConstants.Int);
		classList.add(TreeConstants.Str);
		classList.add(TreeConstants.Bool);
		classList.add(TreeConstants.IO);
		classList.add(TreeConstants.SELF_TYPE);
		Enumeration classElems = cls.getElements();

		// get all vertices from AST
		while (classElems.hasMoreElements()) {
			Class_ temp = (Class_) classElems.nextElement();
			if (temp.getName() == TreeConstants.Int) {
				semantError((class_c) temp).println(
						"Redefinition of basic class Int.");
			} else if (temp.getName() == TreeConstants.Str) {
				semantError((class_c) temp).println(
						"Redefinition of basic class Str.");
			} else if (temp.getName() == TreeConstants.Bool) {
				semantError((class_c) temp).println(
						"Redefinition of basic class Bool.");
			} else if (temp.getName() == TreeConstants.SELF_TYPE) {
				semantError((class_c) temp).println(
						"Redefinition of basic class SELF_TYPE.");
			} else if (temp.getName() == TreeConstants.Object_) {
				semantError((class_c) temp).println(
						"Redefinition of basic class Object.");
			} else {
				if (temp.getName().str.equals("Main")) {
					foundMain = true;
				}
				if (!classList.contains(temp.getName())) {
					SymToClassMap.put(temp.getName(), temp);
					/*
					 * if (classList.indexOf(temp.getParent()) == -1) {
					 * classList.add(temp.getParent()); }
					 */
					if (classList.indexOf(temp.getName()) == -1) {
						classList.add(temp.getName());
					}
				} else {
					semantError((class_c) temp).println(
							"Redefinition of basic class " + temp.getName().str
									+ ".");
				}
			}
		}

		classElems = cls.getElements();
		adjMat = new int[classList.size()][classList.size()];
		// Arrays.fill(adjMat, 0);

		// add all the classes to hashset
		for (int i = 0; i < classList.size(); i++) {
			classListSet.add(classList.get(i));
		}

		for (int i = 0; i < adjMat.length; i++) {
			for (int j = 0; j < adjMat.length; j++) {
				adjMat[i][j] = 0;
			}
		}

		// add edges between baic classes

		adjMat[classList.indexOf(TreeConstants.Object_)][classList
				.indexOf(TreeConstants.Int)] = 1;
		adjMat[classList.indexOf(TreeConstants.Object_)][classList
				.indexOf(TreeConstants.Str)] = 1;
		adjMat[classList.indexOf(TreeConstants.Object_)][classList
				.indexOf(TreeConstants.Bool)] = 1;
		adjMat[classList.indexOf(TreeConstants.Object_)][classList
				.indexOf(TreeConstants.IO)] = 1;

		HashSet<AbstractSymbol> tempSet = new HashSet<>();
		while (classElems.hasMoreElements()) {
			Class_ temp = (Class_) classElems.nextElement();
			if (!tempSet.contains(temp.getName())) {
				if (classList.contains(temp.getParent())) {
					int edge_src = classList.indexOf(temp.getParent());
					int edge_dst = classList.indexOf(temp.getName());
					adjMat[edge_src][edge_dst] = 1;
					tempSet.add(temp.getName());
				}else{
					semantError((class_c) temp).println(
							"Inheriting from undefined class " + temp.getParent().str
									+ ".");
				}
			}
		}

		/*
		 * for(int i=0; i< classList.size(); i++){
		 * System.out.print(classList.get(i).str +" "); }
		 */
		// System.out.println();
		/*
		 * for (int i = 0; i < adjMat.length; i++) { for (int j = 0; j <
		 * adjMat.length; j++) { System.out.print(adjMat[i][j]+" ") ; }
		 * System.out.println(); }
		 */
		// TODO Add checking for inheritance from basic classes Int, Str, Bool
		for (int i = 0; i < 4; i++) {
			int vertexInd = 0;
			switch (i) {
			case 0:
				vertexInd = classList.indexOf(TreeConstants.Int);
				break;
			case 1:
				vertexInd = classList.indexOf(TreeConstants.Str);
				break;
			case 2:
				vertexInd = classList.indexOf(TreeConstants.Bool);
				break;
			case 3:
				vertexInd = classList.indexOf(TreeConstants.SELF_TYPE);
				break;
			}
			for (int j = 0; j < classList.size(); j++) {
				if (adjMat[vertexInd][j] == 1) {
					semantError(
							this.SymToClassMap.get(classList.get(j))
									.getFilename(),
							this.SymToClassMap.get(classList.get(j))).println(
							classList.get(j).str + " inherits basic class "
									+ classList.get(vertexInd).str);
				}
			}
		}

		ArrayList<Integer> ancestors = new ArrayList<>();
		int[] visitedArr = new int[classList.size()];

		visitedArr[classList.indexOf(TreeConstants.Object_)] = 1;
		ancestors.add(classList.indexOf(TreeConstants.Object_));
		DFS(adjMat, visitedArr, classList.indexOf(TreeConstants.Object_),
				ancestors);

		// do DFS for nodes which are not children of Object class which happens
		// only when there is cyclic dependency
		for (int i = 0; i < visitedArr.length; i++) {
			if (visitedArr[i] != 1) {
				// System.out.println(i);
				visitedArr[i] = 1;
				ancestors.add(i);
				DFS(adjMat, visitedArr, i, ancestors);
			}
		}
	}

	public void DFS(int adjMat[][], int visitedArr[], int vertex,
			ArrayList<Integer> ancestors) {

		for (int i = 0; i < adjMat.length; i++) {
			if (adjMat[vertex][i] == 1 && i != vertex) {
				if (visitedArr[i] != 1) {
					visitedArr[i] = 1;
					ancestors.add(i);
					DFS(adjMat, visitedArr, i, ancestors);
					ancestors.remove(ancestors.indexOf(i));
				} else {
					if (ancestors.indexOf(i) != -1) {
						semantError().println("Found cycle");
					} else {
						semantError().println(
								"Found cross edge " + vertex + ":" + i);
					}
				}
			}
		}
	}
}
