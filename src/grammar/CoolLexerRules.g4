lexer grammar CoolLexerRules;
@members{
  public static int maxStringLength = 1024;
  /*public static String fixString(String inp){
    String ret = new String();
    int length = inp.length();
    inp = inp.substring(1,length-1);
    System.out.println("inp "+inp+" length "+inp.length());
    inp = inp.replaceAll("\\t", ""+'\t');
    inp = inp.replaceAll("\\b", ""+'\b');
    inp = inp.replaceAll("\\n", ""+'\n');
    inp = inp.replaceAll("\\\n", ""+'\n');
    inp = inp.replaceAll("\"", ""+'"');
    inp = inp.replaceAll("\\\\", "");
    ret = ret+inp;
    System.out.println("inp "+ret);
    
    return ret;
  }*/
  public static String fixString(String inp){
    String ret = new String();
    int length = inp.length();
    //inp = inp.substring(1,length-1);
    char[] x = inp.toCharArray();
    for(int i=1;i<x.length-1;i++){
        if(x[i]=='\\'){
            switch(x[i+1]){
                case 'b':
                    ret = ret +'\b';
                    break;
                case 't':
                    ret = ret +'\t';
                    break;
                case 'n':
                    ret = ret +'\n';
                    break;
                case 'f':
                    ret = ret +'\f';
                    break;
                default:
                    ret = ret + x[i+1];
                    break;    
            }
            i++;
        }else{
            ret = ret + x[i];
        }
    } 
    return ret;
  }
  public boolean valid(){
    Token t = _factory.create(_tokenFactorySourcePair, _type, _text, _channel, _tokenStartCharIndex, getCharIndex()-1,
    _tokenStartLine, _tokenStartCharPositionInLine);
    //System.out.println("token string is "+t.getText()+" length"+t.getText().length());
    String fixed = fixString(t.getText());
    //System.out.print(fixed);
    //System.out.println("fixed string "+fixed+" length "+fixed.length());
    setText(fixed);
    mode(0);
    if(fixed.length()>maxStringLength){
      System.err.println("String constant too long");
      return false;
    }
    return true;
  }
}

LPAREN      : '(';
RPAREN      : ')';
COLON       : ':';
ATSYM       : '@';
SEMICOLON   : ';';
COMMA       : ',';
PLUS        : '+';
MINUS       : '-';
STAR        : '*';
SLASH       : '/';
TILDE       : '~';
LT          : '<';
EQUALS      : '=';
LBRACE      : '{';
RBRACE      : '}';
DOT         : '.';
DARROW      : '=>';
LTE         : '<=';
ASSIGN      : '<-';

CLASS       : [cC][lL][aA][sS][sS];
ELSE        : [eE][lL][sS][eE];
FALSE  		  : [f][aA][lL][sS][eE];
FI          : [fF][iI];
IF          : [iI][fF];
IN          : [iI][nN];
INHERITS    : [iI][nN][hH][eE][rR][iI][tT][sS];
ISVOID      : [iI][sS][vV][oO][iI][dD];
LET         : [lL][eE][tT];
LOOP        : [lL][oO][oO][pP];
POOL        : [pP][oO][oO][lL];
THEN        : [tT][hH][eE][nN];
WHILE       : [wW][hH][iI][lL][eE];
CASE        : [cC][aA][sS][eE];
ESAC        : [eE][sS][aA][cC];
NEW         : [nN][eE][wW];
NOT         : [nN][oO][tT];
OF          : [oO][fF];
TRUE  		  : [t][rR][uU][eE];

TYPE : ('A'..'Z') ( 'a'..'z' | '0'..'9' | '_' | 'A'..'Z')*;
ID : ('a'..'z') ( 'a'..'z' | '0'..'9' | '_' | 'A'..'Z')*;
Integer : ('0'..'9')+ ;
WS : [ \t\v\r\n]+ -> skip;

LQUOTE:   '"' -> more , mode(STR) ;


SINGLE_COMMENT  : '--'.*? '\n' -> skip;
COMMENT_OPEN    : '(*' -> more,pushMode(COMMENT) ;
COMMENT_CLOSE   : '*)' {System.err.println("Unmatched *) ");};

mode COMMENT;
OPEN  :   '(*' -> more, pushMode(COMMENT);
CLOSE :   '*)' -> more, popMode;
END   :   '\n' EOF {System.err.println("EOF in comment");};
ANY   :   . -> skip;
NL    :   '\n' -> skip;


mode STR;
/*String  : '"' {
                  Token t = _factory.create(_tokenFactorySourcePair, _type, _text, _channel, _tokenStartCharIndex, getCharIndex()-1,
								  _tokenStartLine, _tokenStartCharPositionInLine);
                  //System.out.println("token string is "+t.getText()+" length"+t.getText().length());
                  String fixed = fixString(t.getText());
                  //System.out.print(fixed);
                  //System.out.println("fixed string "+fixed+" length "+fixed.length());
                  setText(fixed);
                  if(fixed.length()>maxStringLength){
                    System.err.println("String constant too long");
                    setType(-3);
                    mode(0);
                  }else{
                    mode(0);
                  }
              };
*/
String  : '"' {valid()}?;
NEWLINE : '\n' {System.err.println("Unterminated String Constat");} -> more,mode(DEFAULT_MODE);
ESC     : '\u0000' {System.err.println("String contains null character");} -> more,mode(ERR);
ESC_NULL: '\\''\u0000' {System.err.println("String escaped contains null character");} -> more,mode(ERR);
STR_EOF : '\n' EOF {System.err.println("EOF in String");} -> more; 
TAB     : '\\t' -> more; 
BACKSPAC: '\\b' -> more;
LINEFEED: '\\f' -> more;
SLASHN  : '\\n' -> more;
ESC_NL  : '\\\n'-> more;
ESC_CHAR: '\\'. -> more;
TEXT    : ~[\n\"\u0000\\]+ -> more;

mode ERR;
ERR1    : '"' -> mode(DEFAULT_MODE);
ERR2    : '\n' -> mode(DEFAULT_MODE);
ERR3    : . -> skip;

//mode DUMMY;
//DUM     : '"'? -> mode(DEFAULT_MODE);
