/***
 * Excerpted from "The Definitive ANTLR 4 Reference",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/tpantlr2 for more book information.
***/
package main;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Nullable;

import java.util.*;

public class ErrorListener extends BaseErrorListener {
	public void syntaxError(Recognizer<?, ?> recognizer,
				Object offendingSymbol,
				int line, int charPositionInLine,
				String msg,
				RecognitionException e)
    {
		
		/*if(msg.contains("<EOF>")){
			if(msg.contains("'<EOF>'"))
				System.err.println("\"" + Parser1.inputFile + "\", line " + line + 
		         ": parse error at or near "+line+":"+charPositionInLine+" "+msg);
		}else*/
		if(!msg.contains("<EOF>"))
			System.err.println("\"" + Parser1.inputFile + "\", line " + line + 
			         ": parse error at or near "+line+":"+charPositionInLine+" "+msg);
        ASTBuilder.encounteredError=true;
        //underlineError(recognizer,(Token)offendingSymbol,
         //              line, charPositionInLine);
    }

    protected void underlineError(Recognizer recognizer,
                                  Token offendingToken, int line,
                                  int charPositionInLine) {
        CommonTokenStream tokens =
            (CommonTokenStream)recognizer.getInputStream();
        String input = tokens.getTokenSource().getInputStream().toString();
        String[] lines = input.split("\n");
        String errorLine = lines[line - 1];
        System.err.println(errorLine);
        for (int i=0; i<charPositionInLine; i++) System.err.print(" ");
        int start = offendingToken.getStartIndex();
        int stop = offendingToken.getStopIndex();
        if ( start>=0 && stop>=0 ) {
            for (int i=start; i<=stop; i++) System.err.print("^");
        }
        System.err.println();
    }
}
