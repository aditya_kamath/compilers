package main;

import java.util.Iterator;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.stringtemplate.v4.compiler.STParser.templateAndEOF_return;

import main.Cool.AssignExprContext;
import main.Cool.Class1Context;
import main.Cool.ClassDefContext;
import main.Cool.ClassesContext;
import main.Cool.CompOperContext;
import main.Cool.ExprBlkContext;
import main.Cool.ExprContext;
import main.Cool.FalseStmntContext;
import main.Cool.FeatureContext;
import main.Cool.FormalContext;
import main.Cool.FuncCallContext;
import main.Cool.FuncParamContext;
import main.Cool.FunctionContext;
import main.Cool.IdStmntContext;
import main.Cool.IfElseBlkContext;
import main.Cool.IntStmntContext;
import main.Cool.IsVoidExprContext;
import main.Cool.LetStmntContext;
import main.Cool.MulDivContext;
import main.Cool.NewObjContext;
import main.Cool.NotExprContext;
import main.Cool.ObjDecInitContext;
import main.Cool.ObjMemFuncCallContext;
import main.Cool.ParEncExprContext;
import main.Cool.PlusMinusContext;
import main.Cool.ProgramContext;
import main.Cool.StrStmntContext;
import main.Cool.SwitchCaseContext;
import main.Cool.TildeExprContext;
import main.Cool.TrueStmntContext;
import main.Cool.WhileLoopContext;

public class ASTBuilder extends CoolBaseVisitor<TreeNode> {
	
	public programc prgm;
	public String fileName;
	public static boolean encounteredError = false;
	
	public int getLineNum(ParserRuleContext ctx){
		
		return ctx.start.getLine();
		
	}
	
	public ASTBuilder(String fileName){
		
		this.fileName = fileName;
	}

	@Override
	public TreeNode visitSwitchCase(SwitchCaseContext ctx) {
		// TODO Auto-generated method stub
		Cases swCases = new Cases(getLineNum(ctx));
		for(int i=0;i<ctx.ID().size();i++){
			AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID(i).getText(),
					ctx.ID(i).getText().length());
			AbstractSymbol type = AbstractTable.idtable.addString(ctx.TYPE(i)
					.getText(), ctx.TYPE(i).getText().length());
			branch caseBranch = new branch(ctx.SEMICOLON(i).getSymbol().getLine(), id, type, (Expression)visit(ctx.expr(i+1)));
			swCases.appendElement(caseBranch);
		}
		
		return new typcase(getLineNum(ctx),(Expression)visit(ctx.expr(0)), swCases);
	}

	@Override
	public TreeNode visitIntStmnt(IntStmntContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol intSymb = AbstractTable.inttable.addString(ctx.Integer()
				.getText(), ctx.Integer().getText().length());
		return new int_const(getLineNum(ctx), intSymb);

	}



	@Override
	public TreeNode visitWhileLoop(WhileLoopContext ctx) {
		// TODO Auto-generated method stub
		return new loop(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
				(Expression) visit(ctx.expr(1)));

	}

	@Override
	public TreeNode visitTildeExpr(TildeExprContext ctx) {
		// TODO Auto-generated method stub
		return new neg(getLineNum(ctx), (Expression) visit(ctx.expr()));

	}

	@Override
	public TreeNode visitFuncParam(FuncParamContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		AbstractSymbol type = AbstractTable.idtable.addString(ctx.TYPE()
				.getText(), ctx.TYPE().getText().length());

		return new formalc(getLineNum(ctx), id, type);
		// return super.visitFuncParam(ctx);
	}

	@Override
	public TreeNode visitStrStmnt(StrStmntContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol strSymb = AbstractTable.stringtable.addString(ctx
				.String().getText(), ctx.String().getText().length());

		return new string_const(getLineNum(ctx), strSymb);

	}

	@Override
	public TreeNode visitNewObj(NewObjContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.TYPE()
				.getText(), ctx.TYPE().getText().length());
		return new new_(getLineNum(ctx), id);

	}



	@Override
	public TreeNode visitIdStmnt(IdStmntContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		return new object(getLineNum(ctx), id);
	}

	@Override
	public TreeNode visitFunction(FunctionContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		AbstractSymbol type = AbstractTable.idtable.addString(ctx.TYPE()
				.getText(), ctx.TYPE().getText().length());
		Formals formalParams = new Formals(getLineNum(ctx));
		for (int i = 0; i < ctx.formal().size(); i++) {
			formalParams.appendElement(visit(ctx.formal(i)));
		}
			return new method(getLineNum(ctx), id, formalParams, type, (Expression)visit(ctx.expr()));
		
	}

	@Override
	public TreeNode visitClassDef(ClassDefContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol type1 = AbstractTable.idtable.addString(ctx.TYPE(0)
				.getText(), ctx.TYPE(0).getText().length());
		AbstractSymbol type2;
		if(ctx.TYPE(1) != null){
		type2 = AbstractTable.idtable.addString(ctx.TYPE(1)
				.getText(), ctx.TYPE(1).getText().length());
		}else{
			type2 = AbstractTable.idtable.addString("Object", "Object".length());
		}
		Features features = new Features(getLineNum(ctx));
		for(int i=0;i < ctx.feature().size();i++){
			features.appendElement(visit(ctx.feature(i)));
		}
		AbstractSymbol fileNameAS = AbstractTable.stringtable.addString(this.fileName, this.fileName.length());
		
		return new class_c(getLineNum(ctx),type1,type2,features,fileNameAS);
		
	}

	@Override
	public TreeNode visitIsVoidExpr(IsVoidExprContext ctx) {
		// TODO Auto-generated method stub
		return new isvoid(getLineNum(ctx), (Expression) visit(ctx.expr()));
		// return super.visitIsVoidExpr(ctx);
	}

	@Override
	public TreeNode visitExprBlk(ExprBlkContext ctx) {
		// TODO Auto-generated method stub
		Expressions exprs = new Expressions(getLineNum(ctx));
		for (int i = 0; i < ctx.expr().size(); i++) {
			exprs.appendElement(visit(ctx.expr(i)));
		}
		return new block(getLineNum(ctx), exprs);
		// return super.visitExprBlk(ctx);
	}

	@Override
	public TreeNode visitLetStmnt(LetStmntContext ctx) {
		// TODO Auto-generated method stub
		
		int idCount = ctx.ID().size();
		ctx.ID(0).getSourceInterval();
		let letStmnt = null;
		int assignCount = ctx.ASSIGN().size();
		for(int i=ctx.getChildCount()-3;i>0;i--){
			if(ctx.getChild(i) instanceof TerminalNode){
				TerminalNode tempTermNode = (TerminalNode)ctx.getChild(i);
				if(tempTermNode.getSymbol().getType() == CoolLexerRules.ID){
					idCount--;
					AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID(idCount).getText(),
							ctx.ID(idCount).getText().length());
					AbstractSymbol type = AbstractTable.idtable.addString(ctx.TYPE(idCount)
							.getText(), ctx.TYPE(idCount).getText().length());
					TerminalNode tempTermAssignNode = (TerminalNode)ctx.getChild(i+3);
					if(tempTermAssignNode.getSymbol().getType() == CoolLexerRules.ASSIGN){
						Expression param = (Expression)visit(ctx.getChild(i+4));
						assignCount--;
						if(letStmnt == null){
							letStmnt = new let(getLineNum(ctx.expr(ctx.expr().size()-1)),id,type,param,(Expression)visit(ctx.expr(ctx.expr().size()-1)));
						}else{
							letStmnt = new let(getLineNum(ctx.expr(ctx.expr().size()-1)),id,type,param,letStmnt);
						}
						
					}else{
						if(letStmnt == null){
							letStmnt = new let(getLineNum(ctx.expr(ctx.expr().size()-1)),id,type,new no_expr(0),(Expression)visit(ctx.expr(ctx.expr().size()-1)));
						}else{
							letStmnt = new let(getLineNum(ctx.expr(ctx.expr().size()-1)),id,type,new no_expr(0),letStmnt);
						}
					}
				}
			}
		}
		return letStmnt;
	}

	@Override
	public TreeNode visitObjMemFuncCall(ObjMemFuncCallContext ctx) {
		// TODO Auto-generated method stub

		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());

		Expressions params = new Expressions(getLineNum(ctx));
		for (int i = 1; i < ctx.expr().size(); i++) {
			params.appendElement(visit(ctx.expr(i)));
		}
		if (ctx.ATSYM() != null) {
			AbstractSymbol superClass = AbstractTable.idtable.addString(ctx
					.TYPE().getText(), ctx.TYPE().getText().length());
			return new static_dispatch(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					superClass, id, params);
		} else {
			return new dispatch(getLineNum(ctx), (Expression) visit(ctx.expr(0)), id, params);
		}

		// return super.visitObjMemFuncCall(ctx);
	}

	@Override
	public TreeNode visitAssignExpr(AssignExprContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		return new assign(getLineNum(ctx), id, (Expression) visit(ctx.expr()));
	}

	@Override
	public TreeNode visitClasses(ClassesContext ctx) {
		// TODO Auto-generated method stub
		if(encounteredError==true){
			prgm = null;
			return prgm;
		}
		Classes classes1 = new Classes(getLineNum(ctx));
		//System.out.println(encounteredError);

		for(int i=0;i<ctx.class1().size();i++){
			classes1.appendElement(visit(ctx.class1(i)));
		}
		prgm = new programc(getLineNum(ctx),classes1);
		return prgm;
	}

	@Override
	public TreeNode visitParEncExpr(ParEncExprContext ctx) {
		// TODO Auto-generated method stub
		return visit(ctx.expr());
	}

	@Override
	public TreeNode visitTrueStmnt(TrueStmntContext ctx) {
		// TODO Auto-generated method stub
		return new bool_const(getLineNum(ctx), true);
	}



	@Override
	public TreeNode visitObjDecInit(ObjDecInitContext ctx) {
		// TODO Auto-generated method stub

		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		AbstractSymbol type = AbstractTable.idtable.addString(ctx.TYPE()
				.getText(), ctx.TYPE().getText().length());
		if (ctx.ASSIGN() != null) {
			return new attr(getLineNum(ctx), id, type, (Expression) visit(ctx.expr()));
		} else {
			return new attr(getLineNum(ctx), id, type, new no_expr(0));
		}

	}



	@Override
	public TreeNode visitIfElseBlk(IfElseBlkContext ctx) {
		// TODO Auto-generated method stub
		return new cond(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
				(Expression) visit(ctx.expr(1)),
				(Expression) visit(ctx.expr(2)));

	}

	@Override
	public TreeNode visitFalseStmnt(FalseStmntContext ctx) {
		// TODO Auto-generated method stub
		return new bool_const(getLineNum(ctx), false);
	}



	@Override
	public TreeNode visitNotExpr(NotExprContext ctx) {
		// TODO Auto-generated method stub
		return new comp(getLineNum(ctx), (Expression) visit(ctx.expr()));
	}

	@Override
	public TreeNode visitFuncCall(FuncCallContext ctx) {
		// TODO Auto-generated method stub
		AbstractSymbol selfId = AbstractTable.idtable.addString("self",
				"self".length());
		AbstractSymbol id = AbstractTable.idtable.addString(ctx.ID().getText(),
				ctx.ID().getText().length());
		Expression selfObj = new object(getLineNum(ctx), selfId);
		Expressions params = new Expressions(getLineNum(ctx));
		for (int i = 0; i < ctx.expr().size(); i++) {
			params.appendElement(visit(ctx.expr(i)));
		}
		return new dispatch(getLineNum(ctx), selfObj, id, params);
	}

	@Override
	public TreeNode visitMulDiv(MulDivContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.SLASH() != null){
			return new divide(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}else{
			return new mul(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}
		
		
	}

	@Override
	public TreeNode visitCompOper(CompOperContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.expr(0).getClass().equals(Cool.CompOperContext.class) ||
				ctx.expr(1).getClass().equals(Cool.CompOperContext.class)){
			this.encounteredError=true;
			if(ctx.expr(0).getClass().equals(Cool.CompOperContext.class)){
			System.err.println("\"" + Parser1.inputFile + "\", line " + getLineNum(ctx.expr(0))+ 
			         ": parse error at or near "+getLineNum(ctx.expr(0)));
			}
			
			if(ctx.expr(1).getClass().equals(Cool.CompOperContext.class)){
				System.err.println("\"" + Parser1.inputFile + "\", line " + getLineNum(ctx.expr(1))+ 
				         ": parse error at or near "+getLineNum(ctx.expr(1)));
				}
			
		}
		if(ctx.LT() != null){
			return new lt(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}else if(ctx.LTE() != null){
			return new leq(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}else{
			return new eq(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}
		
	}

	@Override
	public TreeNode visitPlusMinus(PlusMinusContext ctx) {
		if(ctx.PLUS() != null){
			return new plus(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}else{
			return new sub(getLineNum(ctx), (Expression) visit(ctx.expr(0)),
					(Expression) visit(ctx.expr(1)));
		}
	}

}
