parser grammar Cool;
options{
  tokenVocab = CoolLexerRules;
}
//import CoolLexerRules;

program
    : (class1 SEMICOLON)+ EOF # Classes
    ;

class1
    : CLASS TYPE (INHERITS TYPE)? LBRACE (feature SEMICOLON)* RBRACE # ClassDef
    ;
 
feature
    : ID LPAREN ( formal (COMMA formal)*)? RPAREN COLON TYPE LBRACE expr RBRACE # Function
    | ID COLON TYPE (ASSIGN expr)? # ObjDecInit
    ;

formal
    : ID COLON TYPE # funcParam
    ;

expr
    : expr (ATSYM TYPE)? DOT ID LPAREN ( expr (COMMA expr)* )? RPAREN # objMemFuncCall
    | ID LPAREN ( expr (COMMA expr)*)? RPAREN # funcCall
    | IF expr THEN expr ELSE expr FI # ifElseBlk
    | WHILE expr LOOP expr POOL # whileLoop
    | LBRACE (expr SEMICOLON)+ RBRACE # exprBlk
    | CASE expr OF ( ID COLON TYPE DARROW expr SEMICOLON)+ ESAC # switchCase
    | NEW TYPE # newObj
    | TILDE expr #TildeExpr
    | ISVOID expr # isVoidExpr
    | expr (STAR | SLASH) expr # MulDiv
    | expr (PLUS | MINUS) expr # PlusMinus
    | expr (LTE | LT | EQUALS) expr # CompOper
    | NOT expr # notExpr
    | ID ASSIGN<assoc=right> expr # assignExpr
    | LPAREN expr RPAREN # parEncExpr
    | ID # idStmnt
    | Integer # intStmnt
    | String # strStmnt
    | TRUE # trueStmnt
    | FALSE # falseStmnt
    | LET ID COLON TYPE (ASSIGN expr)? (COMMA ID COLON TYPE (ASSIGN expr)? )* IN expr # letStmnt
    ;

