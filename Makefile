MAKE = make
OUT_JAR = cool.jar
%: make_jar

make_jar:	build
	cp append.ll ./dist/

build:
	$(MAKE) -C ./src/grammar -f Makefile
	$(MAKE) -C ./src/java -f Makefile


clean:
	$(MAKE) -C ./src/grammar -f Makefile clean
	$(MAKE) -C ./src/java -f Makefile clean
	rm -rf ./dist/$(OUT_JAR)


