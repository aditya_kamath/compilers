package main;

import java.util.Enumeration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;
import java.util.HashMap;

;

/** Defines simple phylum Program */
abstract class Program extends TreeNode {
	protected Program(int lineNumber) {
		super(lineNumber);
	}

	public abstract void dump_with_types(PrintStream out, int n);

	public abstract void semant();

	public abstract void cgen(PrintStream s);

}

/** Defines simple phylum Class_ */
abstract class Class_ extends TreeNode {
	protected Class_(int lineNumber) {
		super(lineNumber);
	}

	public abstract void dump_with_types(PrintStream out, int n);

	public abstract AbstractSymbol getName();

	public abstract AbstractSymbol getParent();

	public abstract AbstractSymbol getFilename();

	public abstract Features getFeatures();

}

/**
 * Defines list phylum Classes
 * <p>
 * See <a href="ListNode.html">ListNode</a> for full documentation.
 */
class Classes extends ListNode {
	public final static Class elementClass = Class_.class;

	/** Returns class of this lists's elements */
	public Class getElementClass() {
		return elementClass;
	}

	protected Classes(int lineNumber, Vector elements) {
		super(lineNumber, elements);
	}

	/** Creates an empty "Classes" list */
	public Classes(int lineNumber) {
		super(lineNumber);
	}

	/** Appends "Class_" element to this list */
	public Classes appendElement(TreeNode elem) {
		addElement(elem);
		return this;
	}

	public TreeNode copy() {
		return new Classes(lineNumber, copyElements());
	}
}

/** Defines simple phylum Feature */
abstract class Feature extends TreeNode {
	protected Feature(int lineNumber) {
		super(lineNumber);
	}

	public abstract void dump_with_types(PrintStream out, int n);

}

/**
 * Defines list phylum Features
 * <p>
 * See <a href="ListNode.html">ListNode</a> for full documentation.
 */
class Features extends ListNode {
	public final static Class elementClass = Feature.class;

	/** Returns class of this lists's elements */
	public Class getElementClass() {
		return elementClass;
	}

	protected Features(int lineNumber, Vector elements) {
		super(lineNumber, elements);
	}

	/** Creates an empty "Features" list */
	public Features(int lineNumber) {
		super(lineNumber);
	}

	/** Appends "Feature" element to this list */
	public Features appendElement(TreeNode elem) {
		addElement(elem);
		return this;
	}

	public TreeNode copy() {
		return new Features(lineNumber, copyElements());
	}
}

/** Defines simple phylum Formal */
abstract class Formal extends TreeNode {
	protected Formal(int lineNumber) {
		super(lineNumber);
	}

	public abstract void dump_with_types(PrintStream out, int n);

}

/**
 * Defines list phylum Formals
 * <p>
 * See <a href="ListNode.html">ListNode</a> for full documentation.
 */
class Formals extends ListNode {
	public final static Class elementClass = Formal.class;

	/** Returns class of this lists's elements */
	public Class getElementClass() {
		return elementClass;
	}

	protected Formals(int lineNumber, Vector elements) {
		super(lineNumber, elements);
	}

	/** Creates an empty "Formals" list */
	public Formals(int lineNumber) {
		super(lineNumber);
	}

	/** Appends "Formal" element to this list */
	public Formals appendElement(TreeNode elem) {
		addElement(elem);
		return this;
	}

	public TreeNode copy() {
		return new Formals(lineNumber, copyElements());
	}
}

/** Defines simple phylum Expression */
abstract class Expression extends TreeNode {
	protected Expression(int lineNumber) {
		super(lineNumber);
	}

	private AbstractSymbol type = null;

	public AbstractSymbol get_type() {
		return type;
	}

	public Expression set_type(AbstractSymbol s) {
		type = s;
		return this;
	}

	public abstract void dump_with_types(PrintStream out, int n);

	public void dump_type(PrintStream out, int n) {
		if (type != null) {
			out.println(Utilities.pad(n) + ": " + type.getString());
		} else {
			out.println(Utilities.pad(n) + ": _no_type");
		}
	}

	public abstract void code(PrintStream s);

}

/**
 * Defines list phylum Expressions
 * <p>
 * See <a href="ListNode.html">ListNode</a> for full documentation.
 */
class Expressions extends ListNode {
	public final static Class elementClass = Expression.class;

	/** Returns class of this lists's elements */
	public Class getElementClass() {
		return elementClass;
	}

	protected Expressions(int lineNumber, Vector elements) {
		super(lineNumber, elements);
	}

	/** Creates an empty "Expressions" list */
	public Expressions(int lineNumber) {
		super(lineNumber);
	}

	/** Appends "Expression" element to this list */
	public Expressions appendElement(TreeNode elem) {
		addElement(elem);
		return this;
	}

	public TreeNode copy() {
		return new Expressions(lineNumber, copyElements());
	}
}

/** Defines simple phylum Case */
abstract class Case extends TreeNode {
	protected Case(int lineNumber) {
		super(lineNumber);
	}

	public abstract void dump_with_types(PrintStream out, int n);

}

/**
 * Defines list phylum Cases
 * <p>
 * See <a href="ListNode.html">ListNode</a> for full documentation.
 */
class Cases extends ListNode {
	public final static Class elementClass = Case.class;

	/** Returns class of this lists's elements */
	public Class getElementClass() {
		return elementClass;
	}

	protected Cases(int lineNumber, Vector elements) {
		super(lineNumber, elements);
	}

	/** Creates an empty "Cases" list */
	public Cases(int lineNumber) {
		super(lineNumber);
	}

	/** Appends "Case" element to this list */
	public Cases appendElement(TreeNode elem) {
		addElement(elem);
		return this;
	}

	public TreeNode copy() {
		return new Cases(lineNumber, copyElements());
	}
}

/**
 * Defines AST constructor 'programc'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class programc extends Program {
	protected Classes classes;
	ClassTable classTable;

	// to store map of class to all methods
	private HashMap<AbstractSymbol, HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>> methodEnv = new HashMap<AbstractSymbol, HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>>();
	// to store map of class to all member variables
	private HashMap<AbstractSymbol, SymbolTable> classEnv = new HashMap<>();

	// codegen related global variables
	// TODO: program

	private String int_layout_size = "64";
	private int lextExprDepth = 0;
	private boolean inLetExpr = false;
	private PrintStream cgen_stream;
	private int tempRegIndex = 0;
	HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>> classToAttribCodeSnippets;
	HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>> classToMethodCodeSnippets;
	private int loopNum = 0, ifelseBlkNum = 0;
	private boolean isRetExpr = false;
	// private int exprDepth=0;
	HashMap<AbstractSymbol, Integer> letVarToIndex;
	HashMap<AbstractSymbol, AbstractSymbol> letVarToType;
	HashMap<AbstractSymbol, AbstractSymbol> methodFormalParams;
	HashMap<AbstractSymbol, Integer> methodFormalParamsToIndex;
	private String retType = "";
	private int blockDepth = 0;
	private int letDepth = 0;

	/**
	 * Creates "programc" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for classes
	 */
	public programc(int lineNumber, Classes a1) {
		super(lineNumber);
		classes = a1;
	}

	public TreeNode copy() {
		return new programc(lineNumber, (Classes) classes.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "programc\n");
		classes.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_program");
		for (Enumeration e = classes.getElements(); e.hasMoreElements();) {
			((Class_) e.nextElement()).dump_with_types(out, n + 2);
		}
	}

	/**
	 * This method is the entry point to the semantic checker. You will need to
	 * complete it in programming assignment 4.
	 * <p>
	 * Your checker should do the following two things:
	 * <ol>
	 * <li>Check that the program is semantically correct
	 * <li>Decorate the abstract syntax tree with type information by setting
	 * the type field in each Expression node. (see tree.h)
	 * </ol>
	 * <p>
	 * You are free to first do (1) and make sure you catch all semantic errors.
	 * Part (2) can be done in a second stage when you want to test the complete
	 * compiler.
	 */
	public void semant() {
		/* ClassTable constructor may do some semantic analysis */

		// phase one checks the class duplication and also checks inhritance
		classTable = new ClassTable(classes);
		// one hash map to store functions
		// one for storing the scopes of variables
		// one pass for populating the types of all the expressions
		// second pass for type checking

		// updates maps with basic classes
		initBasicClasses();

		// if there are any errors in phase 1 then exit the program
		if (classTable.errors()) {
			System.err
					.println("Compilation halted due to static semantic errors.");
			System.exit(1);
		}

		// check for existence of Main class if present then check for main
		// class
		checkMain();

		// update the maps pass
		updateMapPass(methodEnv, classEnv);

		// mehod overriding and also mem varibales duplication

		// type checking pass
		typeCheckPass(methodEnv);

		/* some semantic analysis code may go here */

		if (classTable.errors()) {
			System.err
					.println("Compilation halted due to static semantic errors.");
			System.exit(1);
		}
	}

	public void initBasicClasses() {
		Iterator<Class_> classElems = classTable.getBaiscClasses().iterator();
		while (classElems.hasNext()) {
			Class_ currCls = (Class_) classElems.next();
			HashMap<AbstractSymbol, ArrayList<AbstractSymbol>> currClsMethods = new HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>();
			methodEnv.put(currCls.getName(), currClsMethods);
			Features featureSet = currCls.getFeatures();

			Enumeration features = featureSet.getElements();
			SymbolTable currClsSymbolTable = new SymbolTable();
			classEnv.put(currCls.getName(), currClsSymbolTable);
			currClsSymbolTable.enterScope();
			while (features.hasMoreElements()) {
				Feature feature = (Feature) features.nextElement();
				// check whether a feature is attribute or function definition
				if (feature instanceof attr) {
					attr tempAttr = (attr) feature;
					currClsSymbolTable.addId(tempAttr.name, tempAttr.type_decl);

				} else {
					method tempMethod = (method) feature;

					ArrayList<AbstractSymbol> paramList = new ArrayList<>();
					currClsMethods.put(tempMethod.name, paramList);
					visitBasicCLassMethod(paramList, currClsSymbolTable,
							tempMethod);

					// currClsMethods.put(tempMethod.name, value)
				}
			}
		}
	}

	public void checkMain() {
		if (classTable.foundMain) {
			Class_ mainClass = classTable.SymToClassMap.get(TreeConstants.Main);
			Enumeration features = mainClass.getFeatures().getElements();
			while (features.hasMoreElements()) {
				Feature feature = (Feature) features.nextElement();
				// check whether a feature is attribute or function definition
				if (feature instanceof method) {

					method tempMethod = (method) feature;
					if (tempMethod.name.str.equals("main")) {
						return;
					}

					// currClsMethods.put(tempMethod.name, value)
				}
			}
			classTable.semantError().println("No 'main' method in class Main.");

		} else {
			classTable.semantError().println("Class Main is not defined.");
		}
	}

	public void updateMapPass(
			HashMap<AbstractSymbol, HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>> methodEnv,
			HashMap<AbstractSymbol, SymbolTable> classEnv) {
		Enumeration classElems = classes.getElements();
		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();
			HashMap<AbstractSymbol, ArrayList<AbstractSymbol>> currClsMethods = new HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>();
			methodEnv.put(currCls.getName(), currClsMethods);
			Features featureSet = currCls.getFeatures();

			Enumeration features = featureSet.getElements();
			SymbolTable currClsSymbolTable = new SymbolTable();
			classEnv.put(currCls.getName(), currClsSymbolTable);
			currClsSymbolTable.enterScope();
			while (features.hasMoreElements()) {
				Feature feature = (Feature) features.nextElement();
				// check whether a feature is attribute or function definition
				if (feature instanceof attr) {
					attr tempAttr = (attr) feature;
					if (tempAttr.name.str.equals("self")) {
						classTable
								.semantError(currCls.getFilename(), tempAttr)
								.println(
										"'self' cannot be the name of an attribute");
					} else {
						if (classTable.classListSet
								.contains(tempAttr.type_decl)) {
							if (currClsSymbolTable.lookup(tempAttr.name) != null) {

								classTable.semantError(currCls.getFilename(),
										tempAttr)
										.println("attribute redefined");
							}
							if (tempAttr.type_decl == TreeConstants.SELF_TYPE) {
								currClsSymbolTable.addId(tempAttr.name,
										currCls.getName());
							} else {
								currClsSymbolTable.addId(tempAttr.name,
										tempAttr.type_decl);
							}

						} else {
							// error
							classTable.semantError(currCls.getFilename(),
									tempAttr).println("Unknown type");
						}
					}
				} else {
					method tempMethod = (method) feature;

					if (!currClsMethods.containsKey(tempMethod.name)) {
						ArrayList<AbstractSymbol> paramList = new ArrayList<>();
						currClsMethods.put(tempMethod.name, paramList);
						visitMethod(paramList, currClsSymbolTable, tempMethod,
								currCls);
					} else {
						// error
						// do not type check if method already exists
						classTable.semantError(currCls.getFilename(),
								tempMethod).println("method duplication");
					}
					// currClsMethods.put(tempMethod.name, value)
				}
			}
		}
	}

	public void typeCheckPass(
			HashMap<AbstractSymbol, HashMap<AbstractSymbol, ArrayList<AbstractSymbol>>> methodEnv) {
		Enumeration classElems = classes.getElements();
		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();

			Features featureSet = currCls.getFeatures();
			Enumeration features = featureSet.getElements();

			SymbolTable currClsSymbolTable = classEnv.get(currCls.getName());
			HashSet<AbstractSymbol> visitedMethodSet = new HashSet<>();

			while (features.hasMoreElements()) {
				Feature feature = (Feature) features.nextElement();
				// check whether a feature is attribute or function definition
				if (feature instanceof attr) {

					attr tempAttr = (attr) feature;

					if (attrCheck(currCls.getParent(), tempAttr.name) != null) {
						// error
						classTable
								.semantError(currCls.getFilename(), tempAttr)
								.println(
										"attribute already exists in inherited class");
					}
					visitExpr(currCls, currClsSymbolTable, tempAttr.init);
					if (!(tempAttr.init instanceof no_expr)) {
						if (!conform(tempAttr.type_decl,
								tempAttr.init.get_type(), currCls.getName())) {
							classTable.semantError(currCls.getFilename(),
									tempAttr).println(
									"attr conformation failed");
						}

					}

				} else {
					method tempMethod = (method) feature;
					if (!visitedMethodSet.contains(tempMethod.name)) {
						visitedMethodSet.add(tempMethod.name);
						enterMethod(currClsSymbolTable, tempMethod, currCls);
					}
					// if method is redefined then ignore it
					// currClsMethods.put(tempMethod.name, value)
				}
			}
		}
	}

	public AbstractSymbol attrCheck(AbstractSymbol className,
			AbstractSymbol attrId) {
		if (attrId.str.equals("self")) {
			return TreeConstants.SELF_TYPE;
		}
		AbstractSymbol temp = className;
		while (temp != TreeConstants.No_class) {
			if (classEnv.get(temp).lookup(attrId) != null) {
				return (AbstractSymbol) classEnv.get(temp).lookup(attrId);
			}

			temp = classTable.SymToClassMap.get(temp).getParent();
		}
		return null;
	}

	public ArrayList<AbstractSymbol> methodCheck(AbstractSymbol className,
			AbstractSymbol methodId) {
		AbstractSymbol temp = className;
		temp = classTable.SymToClassMap.get(temp).getParent();

		while (temp != TreeConstants.No_class) {

			if (methodEnv.get(temp).containsKey(methodId)) {
				return methodEnv.get(temp).get(methodId);
			}

			temp = classTable.SymToClassMap.get(temp).getParent();
		}
		return null;

	}

	public void visitExpr(Class_ currClass, SymbolTable currClsSymbolTable,
			Expression expr) {
		if (expr instanceof no_expr) { // no expression check
			expr.set_type(TreeConstants.No_type);
		} else if (expr instanceof dispatch) { // dispatch
			AbstractSymbol funcName = ((dispatch) expr).name;
			Expression obj = ((dispatch) expr).expr;
			Expressions exprs = ((dispatch) expr).actual;

			visitExpr(currClass, currClsSymbolTable, obj);
			AbstractSymbol objType;
			// get type and check whether function being called is member
			// of that class or its parent
			// write function to get the class where method is declared
			objType = obj.get_type();
			if (obj.get_type() == TreeConstants.SELF_TYPE) {
				objType = currClass.getName();
			}
			if (obj.get_type() != null
					|| obj.get_type() != TreeConstants.No_type) {
				if (getClassEnvForMethod(funcName, objType) != null) {
					AbstractSymbol methodClass = getClassEnvForMethod(funcName,
							objType);

					ArrayList<AbstractSymbol> formals = methodEnv.get(
							methodClass).get(funcName);
					int i = 0;
					Enumeration params = exprs.getElements();
					while (params.hasMoreElements()) {

						Expression temp = (Expression) params.nextElement();
						visitExpr(currClass, currClsSymbolTable, temp);

						if (!conform(formals.get(i), temp.get_type(),
								currClass.getName())) {
							classTable.semantError(currClass.getFilename(),
									expr).println(
									"expected " + formals.get(i)
											+ " but found " + temp.get_type());
						}
						i++;
						// check type of the temp with foraml param of
						// method with type of
					}
					if (formals.get(formals.size() - 1) == TreeConstants.SELF_TYPE) {
						expr.set_type(obj.get_type());
					} else {
						expr.set_type(formals.get(formals.size() - 1));
					}

				} else {
					classTable.semantError(currClass.getFilename(), expr)
							.println("Method is not found");
					expr.set_type(TreeConstants.Object_);
				}
			} else {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Type of object is undefined");
				expr.set_type(TreeConstants.Object_);
			}

		} else if (expr instanceof static_dispatch) { // static_dispatch
			AbstractSymbol funcName = ((static_dispatch) expr).name;
			Expression obj = ((static_dispatch) expr).expr;
			Expressions exprs = ((static_dispatch) expr).actual;
			AbstractSymbol type = ((static_dispatch) expr).type_name;
			AbstractSymbol objType;

			visitExpr(currClass, currClsSymbolTable, obj);

			objType = obj.get_type();
			if (obj.get_type() == TreeConstants.SELF_TYPE) {
				objType = currClass.getName();
			}
			if (type == TreeConstants.SELF_TYPE) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Static dispatch SELF_TYPE");
				expr.set_type(TreeConstants.Object_);
			} else if (!classTable.classListSet.contains(type)) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Undefine class type " + type);
				expr.set_type(TreeConstants.Object_);
			} else if (conform(type, objType, currClass.getName())) {
				if (obj.get_type() != null
						|| obj.get_type() != TreeConstants.No_type) {
					if (true) {
						if (getClassEnvForMethod(funcName, type) != null) {
							AbstractSymbol methodClass = getClassEnvForMethod(
									funcName, type);

							ArrayList<AbstractSymbol> formals = methodEnv.get(
									methodClass).get(funcName);
							int i = 0;
							Enumeration params = exprs.getElements();
							while (params.hasMoreElements()) {

								Expression temp = (Expression) params
										.nextElement();
								visitExpr(currClass, currClsSymbolTable, temp);

								if (!conform(formals.get(i), temp.get_type(),
										currClass.getName())) {
									classTable.semantError(
											currClass.getFilename(), expr)
											.println(
													"expected "
															+ formals.get(i)
															+ " but found "
															+ temp.get_type());
								}
								i++;
								// TODO check type of the temp with foraml param
								// of
								// method with type of
							}
							if (formals.get(formals.size() - 1) == TreeConstants.SELF_TYPE) {
								expr.set_type(obj.get_type());
							} else {
								expr.set_type(formals.get(formals.size() - 1));
							}

						} else {
							classTable.semantError(currClass.getFilename(),
									expr).println("Method is not found");
							expr.set_type(TreeConstants.Object_);
						}
					} else {

					}
				} else {
					classTable.semantError(currClass.getFilename(), expr)
							.println("Type of object is undefined");
					expr.set_type(TreeConstants.Object_);
				}
			} else {
				classTable.semantError(currClass.getFilename(), expr).println(
						"static type does not conform to the object type");
				expr.set_type(TreeConstants.Object_);
			}

		} else if (expr instanceof bool_const) { // bool constant
			expr.set_type(TreeConstants.Bool);
		} else if (expr instanceof block) { // block of expressions
			block tempExprBlk = (block) expr;
			Enumeration<Expression> exprs = tempExprBlk.body.getElements();
			AbstractSymbol last = null;

			while (exprs.hasMoreElements()) {
				Expression exprTmp = exprs.nextElement();
				visitExpr(currClass, currClsSymbolTable, exprTmp);
				last = exprTmp.get_type();

			}

			if (last != null) {
				tempExprBlk.set_type(last);
			} else {
				tempExprBlk.set_type(TreeConstants.Object_);
				classTable.semantError(currClass.getFilename(), expr).println(
						"Last expr type in blk is null");
			}
		} else if (expr instanceof isvoid) { // isvoid expr
			visitExpr(currClass, currClsSymbolTable, ((isvoid) expr).e1);
			expr.set_type(TreeConstants.Bool);
		} else if (expr instanceof new_) { // new type

			if (classTable.classListSet.contains(((new_) expr).get_type_name())) {
				if (((new_) expr).get_type() == TreeConstants.SELF_TYPE) {
					((new_) expr).set_type(currClass.getName());
				} else {
					((new_) expr).set_type(((new_) expr).get_type_name());
				}

			} else {
				((new_) expr).set_type(TreeConstants.Object_);
			}
		} else if (expr instanceof object) { // object expression check
			// fix this check in parent class member variables

			if ((((object) expr).get_name()) == TreeConstants.self) {
				(expr).set_type(TreeConstants.SELF_TYPE);
			} else if (attrCheck(currClass.getName(),
					(((object) expr).get_name())) != null) {
				expr.set_type(attrCheck(currClass.getName(),
						(((object) expr).get_name())));
			} else {
				classTable.semantError(currClass.getFilename(), expr).println(
						((object) expr).name.str + " not declared in "
								+ currClass.getName() + " scope");
				(expr).set_type(TreeConstants.Object_);

			}
		} else if (expr instanceof string_const) { // string constant check
			expr.set_type(TreeConstants.Str);
		} else if (expr instanceof comp) { // not expr check
			visitExpr(currClass, currClsSymbolTable, ((comp) expr).e1);
			if (((comp) expr).e1.get_type() != TreeConstants.Bool) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Expected bool but found :"
								+ ((comp) expr).e1.get_type().str);

			}
			expr.set_type(TreeConstants.Bool);

		} else if (expr instanceof cond) { // if expr then expr else expr
			visitExpr(currClass, currClsSymbolTable, ((cond) expr).pred);
			if (((cond) expr).pred.get_type() == TreeConstants.Bool) {
				visitExpr(currClass, currClsSymbolTable, ((cond) expr).then_exp);
				visitExpr(currClass, currClsSymbolTable, ((cond) expr).else_exp);
				if (((cond) expr).then_exp.get_type() == TreeConstants.SELF_TYPE
						&& ((cond) expr).else_exp.get_type() == TreeConstants.SELF_TYPE) {
					expr.set_type(TreeConstants.SELF_TYPE);
				} else if (((cond) expr).then_exp.get_type() == TreeConstants.SELF_TYPE) {
					expr.set_type(LCA(currClass.getName(),
							((cond) expr).else_exp.get_type()));
				} else if (((cond) expr).else_exp.get_type() == TreeConstants.SELF_TYPE) {
					expr.set_type(LCA(((cond) expr).then_exp.get_type(),
							currClass.getName()));
				} else {
					expr.set_type(LCA(((cond) expr).then_exp.get_type(),
							((cond) expr).else_exp.get_type()));
				}

			} else {
				classTable.semantError(currClass.getFilename(), expr).println(
						"predicated is not bool");
			}

		} else if (expr instanceof divide) { // expr / expr
			visitExpr(currClass, currClsSymbolTable, ((divide) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((divide) expr).e2);
			if (((divide) expr).e1.get_type() != TreeConstants.Int
					|| ((divide) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of divide are :"
								+ ((divide) expr).e1.get_type().str + " "
								+ ((divide) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Int);

		} else if (expr instanceof plus) { // expr + expr
			visitExpr(currClass, currClsSymbolTable, ((plus) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((plus) expr).e2);
			if (((plus) expr).e1.get_type() != TreeConstants.Int
					|| ((plus) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of plus are :"
								+ ((plus) expr).e1.get_type().str + " "
								+ ((plus) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Int);

		} else if (expr instanceof sub) { // expr - expr
			visitExpr(currClass, currClsSymbolTable, ((sub) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((sub) expr).e2);
			if (((sub) expr).e1.get_type() != TreeConstants.Int
					|| ((sub) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of sub are :"
								+ ((sub) expr).e1.get_type().str + " "
								+ ((sub) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Int);

		} else if (expr instanceof mul) { // expr * expr
			visitExpr(currClass, currClsSymbolTable, ((mul) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((mul) expr).e2);
			if (((mul) expr).e1.get_type() != TreeConstants.Int
					|| ((mul) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of mul are :"
								+ ((mul) expr).e1.get_type().str + " "
								+ ((mul) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Int);

		} else if (expr instanceof neg) { // ~ expr
			visitExpr(currClass, currClsSymbolTable, ((neg) expr).e1);
			if (((neg) expr).e1.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Expected Int but found :"
								+ ((neg) expr).e1.get_type().str);

			}
			expr.set_type(TreeConstants.Int);

		} else if (expr instanceof int_const) { // integer constant
			expr.set_type(TreeConstants.Int);
		} else if (expr instanceof leq) { // expr <= expr
			visitExpr(currClass, currClsSymbolTable, ((leq) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((leq) expr).e2);
			if (((leq) expr).e1.get_type() != TreeConstants.Int
					|| ((leq) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of leq are :"
								+ ((leq) expr).e1.get_type().str + " "
								+ ((leq) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Bool);

		} else if (expr instanceof lt) { // expr < expr
			visitExpr(currClass, currClsSymbolTable, ((lt) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((lt) expr).e2);
			if (((lt) expr).e1.get_type() != TreeConstants.Int
					|| ((lt) expr).e2.get_type() != TreeConstants.Int) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"operands of lt are :" + ((lt) expr).e1.get_type().str
								+ " " + ((lt) expr).e2.get_type().str);
			}
			expr.set_type(TreeConstants.Bool);

		} else if (expr instanceof eq) { // expr = expr
			visitExpr(currClass, currClsSymbolTable, ((eq) expr).e1);
			visitExpr(currClass, currClsSymbolTable, ((eq) expr).e2);

			if (((eq) expr).e1.get_type() == TreeConstants.Int
					|| ((eq) expr).e2.get_type() == TreeConstants.Int
					|| ((eq) expr).e1.get_type() == TreeConstants.Str
					|| ((eq) expr).e2.get_type() == TreeConstants.Str
					|| ((eq) expr).e1.get_type() == TreeConstants.Bool
					|| ((eq) expr).e2.get_type() == TreeConstants.Bool) {

				if (((eq) expr).e1.get_type() == ((eq) expr).e2.get_type()) {
					expr.set_type(TreeConstants.Bool);
				} else {
					classTable.semantError(currClass.getFilename(), expr)
							.println(
									"Expected types to be same but found diff for eq operator "
											+ ((eq) expr).e1.get_type().str
											+ " "
											+ ((eq) expr).e2.get_type().str);
				}

			} // dynamically checked return type of this eq oper is Bool
			expr.set_type(TreeConstants.Bool);

		} else if (expr instanceof let) { // let expression check

			Expression body = ((let) expr).body;
			Expression init = ((let) expr).init;

			if (((let) expr).identifier.str.equals("self")) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"'self' cannot be the variable name.");
			}
			visitExpr(currClass, currClsSymbolTable, init);
			if (init.get_type() != TreeConstants.No_type) {

				if (!conform(((let) expr).type_decl, init.get_type(),
						currClass.getName())) {

					classTable.semantError(currClass.getFilename(), expr)
							.println(
									"identifer " + ((let) expr).identifier.str
											+ " expects type "
											+ ((let) expr).type_decl
											+ " but found " + init.get_type());
				}

			}
			currClsSymbolTable.enterScope();
			if (classTable.classListSet.contains(((let) expr).type_decl)) {

				if (!((let) expr).identifier.equals("self")) {
					currClsSymbolTable.addId(((let) expr).identifier,
							((let) expr).type_decl);
				}

			} else {
				// Error check for type decl
				classTable.semantError(currClass.getFilename(), expr).println(
						"No such class " + ((let) expr).type_decl);
				if (!((let) expr).identifier.equals("self")) {
					currClsSymbolTable.addId(((let) expr).identifier,
							TreeConstants.Object_);
				}
			}

			visitExpr(currClass, currClsSymbolTable, body);
			currClsSymbolTable.exitScope();
			expr.set_type(body.get_type());

		} else if (expr instanceof loop) { // loop expression check
			visitExpr(currClass, currClsSymbolTable, ((loop) expr).pred);
			if (((loop) expr).pred.get_type() == TreeConstants.Bool) {
				visitExpr(currClass, currClsSymbolTable, ((loop) expr).body);

			} else {
				classTable.semantError(currClass.getFilename(), expr).println(
						"predicated expected to be bool");
			}
			expr.set_type(TreeConstants.Object_);

		} else if (expr instanceof typcase) { // switch case expression check
			Cases swCases = ((typcase) expr).cases;
			Expression cond = ((typcase) expr).expr;
			visitExpr(currClass, currClsSymbolTable, cond);

			HashSet<AbstractSymbol> swCasesSet = new HashSet<>();
			Enumeration cases = swCases.getElements();
			branch curBranch;
			AbstractSymbol exprType = null, tmpType;
			// If type of branch is undefined then do not add it to branch
			// for undefined typed do not check for duplications

			while (cases.hasMoreElements()) {
				curBranch = (branch) cases.nextElement();
				if (classTable.classListSet.contains(curBranch.type_decl)) {
					if (swCasesSet.contains(curBranch.type_decl)) {
						classTable.semantError(currClass.getFilename(), expr)
								.println("already contains this element");
					} else {
						swCasesSet.add(curBranch.type_decl);
					}
					currClsSymbolTable.enterScope();
					currClsSymbolTable.addId(curBranch.name,
							curBranch.type_decl);
					visitExpr(currClass, currClsSymbolTable, curBranch.expr);
					currClsSymbolTable.exitScope();

					tmpType = curBranch.expr.get_type();

					if (exprType != null) {
						exprType = LCA(exprType, tmpType);
					} else {
						exprType = tmpType;
					}

				} else {
					classTable.semantError(currClass.getFilename(), expr)
							.println("Unknow type " + curBranch.type_decl.str);

					currClsSymbolTable.enterScope();
					currClsSymbolTable.addId(curBranch.name,
							TreeConstants.Object_);
					visitExpr(currClass, currClsSymbolTable, curBranch.expr);
					currClsSymbolTable.exitScope();
				}
			}

			expr.set_type(exprType);

		} else if (expr instanceof assign) { // assign expression check id <-
												// expression
			assign tempAssign = (assign) expr;

			visitExpr(currClass, currClsSymbolTable, tempAssign.expr);
			// check if name exists or not if not trigger error

			if (tempAssign.name == TreeConstants.self) {
				classTable.semantError(currClass.getFilename(), expr).println(
						"Cannot assign to 'self'.");
				tempAssign.set_type(TreeConstants.SELF_TYPE);

			} else {
				if (attrCheck(currClass.getName(), tempAssign.name) != null) {

					AbstractSymbol idType = attrCheck(currClass.getName(),
							tempAssign.name);

					if (tempAssign.expr.get_type() == TreeConstants.SELF_TYPE
							&& idType == TreeConstants.SELF_TYPE) {
						tempAssign.set_type(TreeConstants.SELF_TYPE);
					} else if (tempAssign.expr.get_type() == TreeConstants.SELF_TYPE) {
						if (conform(
								attrCheck(currClass.getName(), tempAssign.name),
								currClass.getName(), currClass.getName())) {
							tempAssign.set_type(currClass.getName());
						} else {
							classTable.semantError(currClass.getFilename(),
									expr).println("assign type mismatch");
							tempAssign.set_type(TreeConstants.Object_);
						}

					} else if (conform(idType, tempAssign.expr.get_type(),
							currClass.getName())) {
						tempAssign.set_type(tempAssign.expr.get_type());

					} else {
						classTable.semantError(currClass.getFilename(), expr)
								.println("assign type miss match");
						tempAssign.set_type(TreeConstants.Object_);

					}
				} else {
					classTable
							.semantError(currClass.getFilename(), expr)
							.println("Undeclared identifier " + tempAssign.name);
					tempAssign.set_type(TreeConstants.Object_);
				}
			}
		}
	}

	// return parent class given for method and object on which it is called
	public AbstractSymbol getClassEnvForMethod(AbstractSymbol methodId,
			AbstractSymbol classId) {
		AbstractSymbol temp = classId;

		while (temp != TreeConstants.No_class) {
			if (methodEnv.get(temp).keySet().contains(methodId)) {

				return temp;
			}
			temp = classTable.SymToClassMap.get(temp).getParent();
		}
		return null;

	}

	// check whether child conforms to parent or not
	public boolean conform(AbstractSymbol parent, AbstractSymbol child,
			AbstractSymbol currClass) {

		if (parent == TreeConstants.SELF_TYPE
				&& child == TreeConstants.SELF_TYPE) {
			return true;
		} else if (parent == TreeConstants.SELF_TYPE) {
			return false;
		} else if (child == TreeConstants.SELF_TYPE) {
			child = currClass;
		}
		AbstractSymbol temp = child;
		while (temp != TreeConstants.No_class) {
			if (temp == parent) {
				return true;
			}
			temp = classTable.SymToClassMap.get(temp).getParent();
		}
		return false;

	}

	// computes the LCA of A and B
	public AbstractSymbol LCA(AbstractSymbol A, AbstractSymbol B) {
		if (A == B) {
			return A;
		}
		Stack<AbstractSymbol> sA = new Stack<>();
		Stack<AbstractSymbol> sB = new Stack<>();
		AbstractSymbol temp;
		temp = A;
		sA.push(temp);
		while (temp != TreeConstants.No_class) {
			sA.push(classTable.SymToClassMap.get(temp).getParent());
			temp = classTable.SymToClassMap.get(temp).getParent();
		}

		temp = B;
		sB.push(B);
		while (temp != TreeConstants.No_class) {
			sB.push(classTable.SymToClassMap.get(temp).getParent());
			temp = classTable.SymToClassMap.get(temp).getParent();
		}
		// System.out.println(sA+"\n"+sB);
		while (sA.peek() == sB.peek()) {
			sA.pop();
			temp = sB.pop();
			if (sA.isEmpty() || sB.isEmpty()) {
				return temp;
			}
		}
		return temp;

	}

	public void enterMethod(SymbolTable currClsSymbolTable, method currMetthod,
			Class_ currCls) {

		currClsSymbolTable.enterScope();
		Formals formalParams = currMetthod.formals;
		Enumeration params = formalParams.getElements();

		while (params.hasMoreElements()) {

			formalc param = (formalc) params.nextElement();
			currClsSymbolTable.addId(param.name, param.type_decl);

		}
		// do type check on the method body and also update the types of all
		// expressions
		if (methodCheck(currCls.getName(), currMetthod.name) != null) {
			ArrayList<AbstractSymbol> parentMethodParams = methodCheck(
					currCls.getName(), currMetthod.name);
			ArrayList<AbstractSymbol> currentMethodParams = methodEnv.get(
					currCls.getName()).get(currMetthod.name);
			int parentMethodParamSize, currentParamSize;
			parentMethodParamSize = parentMethodParams.size();
			currentParamSize = currentMethodParams.size();

			if (parentMethodParams.get(parentMethodParamSize - 1) == currentMethodParams
					.get(currentParamSize - 1)) {
				if (parentMethodParamSize == currentParamSize) {
					for (int i = 0; i < parentMethodParamSize - 1; i++) {
						if (parentMethodParams.get(i) != currentMethodParams
								.get(i)) {
							classTable.semantError(currCls.getFilename(),
									currMetthod).println(
									"In redefined method "
											+ currMetthod.name.str
											+ " parameter "
											+ parentMethodParams.get(i).str
											+ " is expected but found "
											+ currentMethodParams.get(i).str);

						}
					}
				} else {
					classTable
							.semantError(currCls.getFilename(), currMetthod)
							.println(
									"In redefined method "
											+ currMetthod.name.str
											+ " number of parameters are diffrent.");

				}
			} else {
				classTable
						.semantError(currCls.getFilename(), currMetthod)
						.println(
								"In redefined method "
										+ currMetthod.name.str
										+ " return type is "
										+ currentMethodParams
												.get(currentParamSize - 1).str
										+ "is not same as the return type "
										+ parentMethodParams
												.get(parentMethodParamSize - 1).str
										+ " of original method");
			}
		}

		visitExpr(currCls, currClsSymbolTable, currMetthod.expr);
		// TODO if else for conformation of re type
		if (!conform(currMetthod.return_type, currMetthod.expr.get_type(),
				currCls.getName())) {
			classTable.semantError(currCls.getFilename(), currMetthod).println(
					"Infered return type " + currMetthod.expr.get_type()
							+ " but the return type is "
							+ currMetthod.return_type);
		}

		currClsSymbolTable.exitScope();

	}

	public void visitMethod(ArrayList<AbstractSymbol> paramList,
			SymbolTable currClsSymbolTable, method currMetthod, Class_ currClass) {

		currClsSymbolTable.enterScope();
		Formals formalParams = currMetthod.formals;
		Enumeration params = formalParams.getElements();
		HashSet<AbstractSymbol> paramsSet = new HashSet<>();
		while (params.hasMoreElements()) {
			formalc param = (formalc) params.nextElement();
			if (param.type_decl == TreeConstants.SELF_TYPE) {
				classTable.semantError(currClass.getFilename(), param).println(
						"Formal parameter cannot have SELF_TYPE");
			}
			if (classTable.classListSet.contains(param.type_decl)) {
				paramList.add(param.type_decl);
				if (param.name.str.equals("self")) {
					classTable.semantError(currClass.getFilename(), param)
							.println("self cannot be formal parameter name");
				} else {
					currClsSymbolTable.addId(param.name, param.type_decl);
					if (paramsSet.contains(param.name)) {
						// TODO flag error
						classTable.semantError(currClass.getFilename(), param)
								.println("Duplication of parameter name");
					} else {
						paramsSet.add(param.name);
					}
				}
			} else {
				classTable.semantError(currClass.getFilename(), param).println(
						"Type not found for formal param "
								+ param.type_decl.str);
				paramList.add(TreeConstants.Object_);
			}

		}
		if (classTable.classListSet.contains(currMetthod.return_type)) {
			paramList.add(currMetthod.return_type);
		} else {
			classTable.semantError(currClass.getFilename(), currMetthod)
					.println(
							"Undefined class" + currMetthod.return_type.str
									+ " used for return type");
			paramList.add(TreeConstants.Object_);
		}

		// visitEpressionAndUpdateType(currMetthod.expr, currClsSymbolTable);
		currClsSymbolTable.exitScope();

	}

	public void visitBasicCLassMethod(ArrayList<AbstractSymbol> paramList,
			SymbolTable currClsSymbolTable, method currMetthod) {

		currClsSymbolTable.enterScope();
		Formals formalParams = currMetthod.formals;
		Enumeration params = formalParams.getElements();
		while (params.hasMoreElements()) {
			formalc param = (formalc) params.nextElement();

			currClsSymbolTable.addId(param.name, param.type_decl);
			paramList.add(param.type_decl);

		}
		paramList.add(currMetthod.return_type);
		// visitEpressionAndUpdateType(currMetthod.expr, currClsSymbolTable);
		currClsSymbolTable.exitScope();

	}

	public AbstractSymbol getParentClass(AbstractSymbol currClass) {
		for (int i = 0; i < classTable.classList.size(); i++) {
			if (classTable.adjMat[i][classTable.classList.indexOf(currClass)] == 1) {
				return classTable.classList.get(i);
			}
		}
		return null;
	}

	/**
	 * This method is the entry point to the code generator. All of the work of
	 * the code generator takes place within CgenClassTable constructor.
	 * 
	 * @param s
	 *            the output stream
	 * @see CgenClassTable
	 * */
	public void cgen(PrintStream s) {
		cgen_stream = s;
		// CgenClassTable codegen_classtable = new CgenClassTable(classes, s);
		// HashMap<AbstractSymbol,Integer> strSymToStrVar = new
		// HashMap<AbstractSymbol,Integer>();
		cgen_stream
				.println("target datalayout = \"e-m:o-i64:64-f80:128-n8:16:32:64-S128\"");
		cgen_stream.println("target triple = \"x86_64-apple-macosx10.10.0\"");

		genBasicTypes();
		generateAllClassLabelStrings();
		generateAllStrings(s);
		generateAllFunctionProto();
		classToAttribCodeSnippets = new HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>>();
		classToMethodCodeSnippets = new HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>>();
		methodFormalParams = new HashMap<AbstractSymbol, AbstractSymbol>();
		methodFormalParamsToIndex = new HashMap<AbstractSymbol, Integer>();

		createClassAtribTypeCodeSnippets(classToAttribCodeSnippets, s);
		s.println();
		createClassFuncTypeCodeSnippets(classToMethodCodeSnippets, s);

		System.out.println(classToAttribCodeSnippets.toString());
		System.out.println(classToMethodCodeSnippets.toString());

		createMethodBodyCodeSnippets(s);
		genMain();
		genContructors();
		genSelfTypeFunctions();
		try {
			addAllBasicFuncs();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// TODO code generator

	/*
	 * first pass to create the code snippet of all attributes
	 */

	public void generateAllStrings(PrintStream s) {

		s.println("@.str = private unnamed_addr constant [22 x i8] c\"Aborting in class %s\\0A\\00\", align 1");

		Enumeration allStrings = AbstractTable.stringtable.getSymbols();

		while (allStrings.hasMoreElements()) {
			AbstractSymbol tempSymb = (AbstractSymbol) allStrings.nextElement();
			String temp = tempSymb.str;
			// @.str = private unnamed_addr constant [22 x i8]
			// c"Aborting in class %s\0A\00", align 1
			s.println("@.str" + tempSymb.index
					+ " = private unnamed_addr constant ["
					+ (int) (temp.length() + 1) + " x i8] c\""
					+ temp.replace("\n", "\\0A") + "\\00\", align 1");

		}
		s.println();
	}

	public void generateAllClassLabelStrings() {
		cgen_stream
				.println("@null_str = global [1 x i8] zeroinitializer, align 1");
		cgen_stream.println("@int_str = global [3 x i8] c\"%d\\00\", align 1");
		cgen_stream.println("@str_str = global [3 x i8] c\"%s\\00\", align 1");
		Iterator<AbstractSymbol> classElems = classTable.classListSet
				.iterator();
		while (classElems.hasNext()) {
			AbstractSymbol temp = classElems.next();
			// @Object_name = global [7 x i8] c"Object\00", align 1
			cgen_stream.println("@" + temp.str + "_name = global ["
					+ (int) (temp.str.length() + 1) + " x i8] c\"" + temp.str
					+ "\\00\", align 1");
		}

	}

	public void generateAllFunctionProto() {
		// @Int_functions_proto = common global %struct.Int_functions
		// zeroinitializer, align 8
		Iterator<AbstractSymbol> classElems = classTable.classListSet
				.iterator();
		while (classElems.hasNext()) {
			AbstractSymbol temp = classElems.next();
			if (temp != TreeConstants.SELF_TYPE) {
				cgen_stream.println("@" + temp.str
						+ "_functions_proto = common global %struct."
						+ temp.str + "_functions zeroinitializer, align 8");
			}
		}

	}

	private void genBasicTypes() {

		cgen_stream
				.println("%struct.Object_functions = type { %struct.Object* (...)*, %struct.String* (...)*, %struct.Object* (...)* }");
		cgen_stream
				.println("%struct.String = type { %struct.Object_functions*, i32, i8*, i8*, i32 }");
		cgen_stream
				.println("%struct.Object = type { %struct.Object_functions*, i32, i8* }");
		cgen_stream
				.println("%struct.Int_functions = type { %struct.Object* (...)*, %struct.String* (...)*, %struct.Int* (...)* }");
		cgen_stream
				.println("%struct.Int = type { %struct.Object_functions*, i32, i8*, i32 }");
		cgen_stream
				.println("%struct.Bool_functions = type { %struct.Object* (...)*, %struct.String* (...)*, %struct.Bool* (...)* }");
		cgen_stream
				.println("%struct.Bool = type { %struct.Object_functions*, i32, i8*, i32 }");
		cgen_stream
				.println("%struct.String_functions = type { %struct.Object* (...)*, %struct.String* (...)*, %struct.String* (...)*, %struct.Int* (...)*, %struct.String* (...)*, %struct.String* (...)* }");
		cgen_stream
				.println("%struct.IO_functions = type { %struct.Object* (...)*, %struct.String* (...)*, %struct.IO* (...)*, %struct.IO* (...)*, %struct.IO* (...)*, %struct.String* (...)*, %struct.Int* (...)* }");
		cgen_stream
				.println("%struct.IO = type { %struct.Object_functions*, i32, i8* }");

	}

	public void createClassAtribTypeCodeSnippets(
			HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>> classToAttribCodeSnippets,
			PrintStream s) {
		Enumeration classElems = classes.getElements();
		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();

			HashMap<AbstractSymbol, Integer> attribToSnippet = new HashMap<AbstractSymbol, Integer>();
			classToAttribCodeSnippets.put(currCls.getName(), attribToSnippet);
			s.print("%struct." + currCls.getName().str
					+ " = type { %struct.Object_functions*, i32, i8*");
			generateAttribTypeStruct(currCls.getName(), currCls.getName(),
					attribToSnippet, s);
			s.println(" }");

		}

	}

	public void createClassFuncTypeCodeSnippets(
			HashMap<AbstractSymbol, HashMap<AbstractSymbol, Integer>> classToMethodCodeSnippets,
			PrintStream s) {
		Enumeration classElems = classes.getElements();
		classElems = classes.getElements();

		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();

			HashMap<AbstractSymbol, Integer> methodToFuncProto = new HashMap<AbstractSymbol, Integer>();
			classToMethodCodeSnippets.put(currCls.getName(), methodToFuncProto);
			s.print("%struct." + currCls.getName().str + "_functions = type { ");
			generateFucTypeStruct(currCls.getName(), currCls.getName(),
					methodToFuncProto, s);
			s.println(" }");

		}

		Class_ currCls = (Class_) classTable.Str_class;

		HashMap<AbstractSymbol, Integer> methodToFuncProto = new HashMap<AbstractSymbol, Integer>();
		classToMethodCodeSnippets.put(currCls.getName(), methodToFuncProto);
		generateBasicFucTypeStruct(currCls.getName(), currCls.getName(),
				methodToFuncProto);

		currCls = (Class_) classTable.IO_class;

		methodToFuncProto = new HashMap<AbstractSymbol, Integer>();
		classToMethodCodeSnippets.put(currCls.getName(), methodToFuncProto);
		generateBasicFucTypeStruct(currCls.getName(), currCls.getName(),
				methodToFuncProto);

	}

	public void createMethodBodyCodeSnippets(PrintStream s) {
		Enumeration classElems = classes.getElements();
		classElems = classes.getElements();

		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();

			generateTempExprs(currCls.getName(), s);

		}
	}

	/**
	 * Recursively call parent till it reaches Object and then populate the
	 * 
	 */

	private int generateAttribTypeStruct(AbstractSymbol currClass,
			AbstractSymbol parentClass,
			HashMap<AbstractSymbol, Integer> attribToSnippet, PrintStream s) {
		int index;
		if (parentClass == TreeConstants.Object_) {
			index = 3;
		} else {
			index = generateAttribTypeStruct(currClass,
					classTable.SymToClassMap.get(parentClass).getParent(),
					attribToSnippet, s);
		}

		Class_ currCls = classTable.SymToClassMap.get(parentClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof attr) {
				attr tempAttr = (attr) feature;
				String attribType;
				if (tempAttr.type_decl == TreeConstants.SELF_TYPE) {
					attribType = "%struct." + currClass.str + "*";
					attribToSnippet.put(tempAttr.name, index);
				} else if (tempAttr.type_decl == TreeConstants.Int
						|| tempAttr.type_decl == TreeConstants.Bool
						|| tempAttr.type_decl == TreeConstants.Str) {
					attribType = "%struct." + tempAttr.type_decl.str + "*";
					attribToSnippet.put(tempAttr.name, index);

				} else {
					attribType = "%struct." + tempAttr.type_decl.str + "*";
					attribToSnippet.put(tempAttr.name, index);
				}
				if (index == 0) {
					s.print(attribType);
				} else {
					s.print(", " + attribType);
				}

				index++;

			}
		}
		return index;
	}

	private int generateFucTypeStruct(AbstractSymbol currClass,
			AbstractSymbol parentClass,
			HashMap<AbstractSymbol, Integer> methodToFuncProto, PrintStream s) {
		int index;
		if (parentClass == TreeConstants.Object_) {
			index = 0;
		} else {
			index = generateFucTypeStruct(currClass, classTable.SymToClassMap
					.get(parentClass).getParent(), methodToFuncProto, s);
		}

		Class_ currCls = classTable.SymToClassMap.get(parentClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof method) {
				method tempMethod = (method) feature;
				String methodType;
				if (tempMethod.return_type == TreeConstants.SELF_TYPE) {
					methodType = "%struct." + currClass.str + "* (...)*";
					methodToFuncProto.put(tempMethod.name, index);
				} else if (tempMethod.return_type == TreeConstants.Int
						|| tempMethod.return_type == TreeConstants.Bool
						|| tempMethod.return_type == TreeConstants.Str) {
					methodType = "%struct." + tempMethod.return_type.str
							+ "* (...)*";
					methodToFuncProto.put(tempMethod.name, index);

				} else {
					methodType = "%struct." + tempMethod.return_type.str
							+ "* (...)*";
					if (methodToFuncProto.containsKey(tempMethod.name)) {
						index--;
					} else {
						methodToFuncProto.put(tempMethod.name, index);
					}
				}
				if (index == 0) {
					s.print(methodType);
				} else {
					s.print(", " + methodType);
				}

				index++;

			}
			/*
			 * else { method tempMethod = (method) feature; if
			 * (tempMethod.return_type == TreeConstants.Int ||
			 * tempMethod.return_type == TreeConstants.Bool ||
			 * tempMethod.return_type == TreeConstants.Str) {
			 * 
			 * String methodSnippet = "%struct." + tempMethod.return_type +
			 * " (...)*"; methodToFuncProto.put(tempMethod.name, methodSnippet);
			 * } else { String methodSnippet = "%struct." +
			 * tempMethod.return_type + "* (...)*";
			 * methodToFuncProto.put(tempMethod.name, methodSnippet); }
			 * 
			 * }
			 */
		}
		return index;
	}

	private int generateBasicFucTypeStruct(AbstractSymbol currClass,
			AbstractSymbol parentClass,
			HashMap<AbstractSymbol, Integer> methodToFuncProto) {
		int index;
		if (parentClass == TreeConstants.Object_) {
			index = 0;
		} else {
			index = generateBasicFucTypeStruct(currClass,
					classTable.SymToClassMap.get(parentClass).getParent(),
					methodToFuncProto);
		}

		Class_ currCls = classTable.SymToClassMap.get(parentClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof method) {
				method tempMethod = (method) feature;
				String methodType;
				System.out.println("return type is " + tempMethod.return_type);
				if (tempMethod.return_type == TreeConstants.SELF_TYPE) {
					methodType = "%struct." + currClass.str + "* (...)*";
					methodToFuncProto.put(tempMethod.name, index);
				} else if (tempMethod.return_type == TreeConstants.Int
						|| tempMethod.return_type == TreeConstants.Bool
						|| tempMethod.return_type == TreeConstants.Str) {
					methodType = "%struct." + tempMethod.return_type.str
							+ "* (...)*";
					methodToFuncProto.put(tempMethod.name, index);

				} else {
					methodType = "%struct." + tempMethod.return_type.str
							+ "* (...)*";
					if (methodToFuncProto.containsKey(tempMethod.name)) {
						index--;
					} else {
						methodToFuncProto.put(tempMethod.name, index);
					}
				}

				index++;

			}
			/*
			 * else { method tempMethod = (method) feature; if
			 * (tempMethod.return_type == TreeConstants.Int ||
			 * tempMethod.return_type == TreeConstants.Bool ||
			 * tempMethod.return_type == TreeConstants.Str) {
			 * 
			 * String methodSnippet = "%struct." + tempMethod.return_type +
			 * " (...)*"; methodToFuncProto.put(tempMethod.name, methodSnippet);
			 * } else { String methodSnippet = "%struct." +
			 * tempMethod.return_type + "* (...)*";
			 * methodToFuncProto.put(tempMethod.name, methodSnippet); }
			 * 
			 * }
			 */
		}
		return index;
	}

	private void generateTempExprs(AbstractSymbol currClass, PrintStream s) {

		Class_ currCls = classTable.SymToClassMap.get(currClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof method) {

				method tempMethod = (method) feature;
				tempRegIndex = 1;
				loopNum = 0;
				ifelseBlkNum = 0;
				isRetExpr = true;
				blockDepth = 0;
				letDepth = 0;
				inLetExpr = false;

				methodFormalParams.clear();
				methodFormalParamsToIndex.clear();
				// exprDepth=0;
				retType = tempMethod.return_type.str;
				if (tempMethod.return_type == TreeConstants.SELF_TYPE) {
					retType = currCls.getName().toString();
				}
				s.print("define %struct." + retType + "* @"
						+ currCls.getName().str + "_" + tempMethod.name.str
						+ " (i8* %self_object");
				Formals formalParams = tempMethod.formals;
				Enumeration params = formalParams.getElements();
				while (params.hasMoreElements()) {
					formalc param = (formalc) params.nextElement();
					s.print(", %struct." + param.type_decl.str + "* %"
							+ param.name.str);
					methodFormalParams.put(param.name, param.type_decl);

				}
				s.println("){");
				params = formalParams.getElements();
				while (params.hasMoreElements()) {
					formalc param = (formalc) params.nextElement();
					s.println("%" + tempRegIndex + " = alloca %struct."
							+ param.type_decl.str + "*");
					methodFormalParamsToIndex.put(param.name, tempRegIndex);
					tempRegIndex++;
					s.println("store %struct." + param.type_decl.str + "* %"
							+ param.name.str + ", %struct."
							+ param.type_decl.str + "** %"
							+ (int) (tempRegIndex - 1));
				}
				visitExpr(currCls, tempMethod.expr);
				s.println("}");
			}
		}

	}

	/**
	 * Given Abstract Symbol of Class creates structure of that object by
	 * traversing till parent
	 */

	// lext expr depth will decide the name of the variable

	public int visitExpr(Class_ currClass,

	Expression expr) {

		if (expr instanceof no_expr) { // no expression check
			cgen_stream.println(";" + expr.lineNumber);
			expr.set_type(TreeConstants.No_type);
		} else if (expr instanceof dispatch) { // dispatch
			cgen_stream.println(";" + expr.lineNumber + " "
					+ ((dispatch) expr).name.str);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;
			String objectTypeString;
			AbstractSymbol funcName = ((dispatch) expr).name;
			Expression obj = ((dispatch) expr).expr;
			Expressions exprs = ((dispatch) expr).actual;

			int objectReg = visitExpr(currClass, obj);
			AbstractSymbol objType;
			// get type and check whether function being called is member
			// of that class or its parent
			// write function to get the class where method is declared
			objType = obj.get_type();
			if (obj.get_type() == TreeConstants.SELF_TYPE) {
				objType = currClass.getName();
			}
			System.out.println("objtype is " + objType.str);
			AbstractSymbol methodClass = getClassEnvForMethod(funcName, objType);

			ArrayList<AbstractSymbol> formals = methodEnv.get(methodClass).get(
					funcName);

			ArrayList<Integer> regs = new ArrayList<Integer>();
			ArrayList<AbstractSymbol> regTyp = new ArrayList<>();
			Enumeration params = exprs.getElements();
			while (params.hasMoreElements()) {

				Expression temp = (Expression) params.nextElement();
				regs.add(visitExpr(currClass, temp));
				regTyp.add(temp.get_type());
			}

			String dispatchReturnType = formals.get(formals.size() - 1).str;
			if (formals.get(formals.size() - 1) == TreeConstants.SELF_TYPE) {
				dispatchReturnType = objType.str;
			}

			System.out.println("metjod is " + funcName.str
					+ " dispatchReturnType is " + dispatchReturnType);
			System.out.println((objType.str));
			int functionInd = (classToMethodCodeSnippets.get(objType))
					.get(funcName);

			// objectReg

			cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
					+ objType.str + "* %" + objectReg + " to i8* ");
			int objectRegCastToVoid = tempRegIndex;
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct." + objType.str
					+ "* %" + objectReg + ", i32 0, i32 0");
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex
					+ " = load %struct.Object_functions** %"
					+ (int) (tempRegIndex - 1));
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex
					+ " = bitcast %struct.Object_functions* %"
					+ (int) (tempRegIndex - 1) + " to %struct." + objType.str
					+ "_functions* ");
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct." + objType.str
					+ "_functions* %" + (int) (tempRegIndex - 1)
					+ ", i32 0, i32 " + functionInd);
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = load %struct."
					+ dispatchReturnType + "* (...)** %"
					+ (int) (tempRegIndex - 1));
			tempRegIndex++;

			if (funcName == TreeConstants.substr
					|| funcName == TreeConstants.concat
					|| funcName == TreeConstants.length) {
				cgen_stream.print("%" + tempRegIndex + " = call %struct."
						+ dispatchReturnType + "*  @String_" + funcName.str
						+ "(i8* %" + objectRegCastToVoid);
			} else if (objType == TreeConstants.IO) {
				if (funcName == TreeConstants.in_int
						|| funcName == TreeConstants.out_string
						|| funcName == TreeConstants.out_int
						|| funcName == TreeConstants.in_string) {
					cgen_stream.print("%" + tempRegIndex + " = call %struct."
							+ dispatchReturnType + "*  @IO_" + funcName.str
							+ "(i8* %" + objectRegCastToVoid);
				}
			} else {
				cgen_stream.print("%" + tempRegIndex + " = call %struct."
						+ dispatchReturnType + "* (...)*  %"
						+ (int) (tempRegIndex - 1) + "(i8* %"
						+ objectRegCastToVoid);
			}

			for (int i = 0; i < formals.size() - 1; i++) {
				cgen_stream.print(", %struct." + regTyp.get(i).str + "* %"
						+ regs.get(i));
			}

			cgen_stream.println(")");
			int callIndex = tempRegIndex;
			tempRegIndex++;

			if (tempIsRet) {
				addRetStmnt(retType, dispatchReturnType, tempRegIndex - 1);
			}

			return callIndex;

		} else if (expr instanceof static_dispatch) { // static_dispatch
			cgen_stream.println(";" + expr.lineNumber + " "
					+ ((static_dispatch) expr).name.str);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;
			String objectTypeString;
			AbstractSymbol funcName = ((static_dispatch) expr).name;
			Expression obj = ((static_dispatch) expr).expr;
			Expressions exprs = ((static_dispatch) expr).actual;
			AbstractSymbol type = ((static_dispatch) expr).type_name;

			int objectReg = visitExpr(currClass, obj);
			AbstractSymbol objType;
			// get type and check whether function being called is member
			// of that class or its parent
			// write function to get the class where method is declared
			objType = obj.get_type();
			if (obj.get_type() == TreeConstants.SELF_TYPE) {
				objType = currClass.getName();
			}
			AbstractSymbol methodClass = getClassEnvForMethod(funcName, type);

			ArrayList<AbstractSymbol> formals = methodEnv.get(methodClass).get(
					funcName);

			ArrayList<Integer> regs = new ArrayList<Integer>();
			ArrayList<AbstractSymbol> regTyp = new ArrayList<>();
			Enumeration params = exprs.getElements();
			while (params.hasMoreElements()) {

				Expression temp = (Expression) params.nextElement();
				regs.add(visitExpr(currClass, temp));
				regTyp.add(temp.get_type());
			}

			String dispatchReturnType = formals.get(formals.size() - 1).str;
			if (formals.get(formals.size() - 1) == TreeConstants.SELF_TYPE) {
				// dispatchReturnType = objType.str;
				dispatchReturnType = type.str;
			}

			// objectReg

			cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
					+ objType.str + "* %" + objectReg + " to i8* ");
			int objectRegCastToVoid = tempRegIndex;
			tempRegIndex++;

			cgen_stream.print("%" + tempRegIndex + " = call %struct."
					+ dispatchReturnType + "*  @" + methodClass.str + "_"
					+ funcName.str + "(i8* %" + objectRegCastToVoid);

			for (int i = 0; i < formals.size() - 1; i++) {
				cgen_stream.print(", %struct." + regTyp.get(i).str + "* %"
						+ regs.get(i));
			}

			cgen_stream.println(")");
			int callIndex = tempRegIndex;
			tempRegIndex++;

			if (tempIsRet) {
				addRetStmnt(retType, dispatchReturnType, tempRegIndex - 1);
			}

			return callIndex;

		} else if (expr instanceof bool_const) { // bool constant
			// expr.set_type(TreeConstants.Bool);
			cgen_stream.println(";" + expr.lineNumber + " "
					+ ((bool_const) expr).val);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = zext i1 "
					+ ((bool_const) expr).val.toString() + " to i32");
			tempRegIndex++;
			cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
					+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Bool", objRegister);
			}
			return objRegister;
		} else if (expr instanceof block) { // block of expressions
			cgen_stream.println(";" + expr.lineNumber);
			block tempExprBlk = (block) expr;
			Enumeration<Expression> exprs = tempExprBlk.body.getElements();
			int last = -1;
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			blockDepth++;
			while (exprs.hasMoreElements()) {
				Expression exprTmp = exprs.nextElement();
				// visitExpr(currClass, currClsSymbolTable, exprTmp);
				if (!exprs.hasMoreElements()) {
					if (tempIsRet) {
						isRetExpr = true;
					} else {
						isRetExpr = false;
					}
				} else {
					isRetExpr = false;
				}
				last = visitExpr(currClass, exprTmp);
			}
			blockDepth--;
			return last;
		} else if (expr instanceof isvoid) { // isvoid expr
			cgen_stream.println(";" + expr.lineNumber);
			// visitExpr(currClass, currClsSymbolTable, ((isvoid) expr).e1);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((isvoid) expr).e1);

			cgen_stream.println("%" + tempRegIndex + " = icmp eq i32** %" + e1
					+ ", null");
			int resReg = tempRegIndex;

			tempRegIndex++;

			cgen_stream.println();

			int objRegister = genDynamicObjectCode("Bool");

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = zext i1 %" + resReg
					+ " to i32");
			tempRegIndex++;

			cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
					+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Bool", objRegister);
			}

			return objRegister;

		} else if (expr instanceof new_) { // new type
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			if (((new_) expr).get_type() == TreeConstants.SELF_TYPE) {
				int temp = genDynamicObjectCode(currClass.getName().str);
				if (tempIsRet) {
					addRetStmnt(retType, currClass.getName().str, temp);

				}
				return temp;
			} else {
				int temp = genDynamicObjectCode(((new_) expr).get_type_name().str);
				if (tempIsRet) {
					addRetStmnt(retType, ((new_) expr).get_type_name().str,
							temp);

				}
				return temp;
			}

		} else if (expr instanceof object) { // object expression check
			// fix this check in parent class member variables
			cgen_stream.println(";" + expr.lineNumber + " Object");
			System.out.println("line num :" + expr.lineNumber);
			System.out.println(letVarToIndex);
			// System.err.println(((object)expr).get_name()+" "+((object)expr).get_type());
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;
			// handle if object is self type
			if ((((object) expr).get_type()) == TreeConstants.SELF_TYPE) {

				cgen_stream.println("%" + tempRegIndex
						+ " = bitcast i8* %self_object" + " to %struct."
						+ currClass.getName().str + "*");
				int objReg = tempRegIndex;
				tempRegIndex++;
				if (tempIsRet) {
					addRetStmnt(retType, currClass.getName().str,
							(tempRegIndex - 1));
				}
				return objReg;
			} else {
				if (inLetExpr
						&& letVarToIndex.containsKey(((object) expr).name)) {
					int index = letVarToIndex.get(((object) expr).name);
					cgen_stream.println("%" + tempRegIndex + " = load %struct."
							+ letVarToType.get(((object) expr).name).str
							+ "** %" + index + ", align 8");

					int objReg = tempRegIndex;
					tempRegIndex++;
					if (tempIsRet) {
						addRetStmnt(retType, ((object) expr).get_type().str,
								(tempRegIndex - 1));
					}
					return objReg;
				} else if (methodFormalParams.containsKey(((object) expr).name)) {
					cgen_stream.println("%"
							+ tempRegIndex
							+ " = load %struct."
							+ ((object) expr).get_type()
							+ "** %"
							+ methodFormalParamsToIndex
									.get(((object) expr).name));
					int objReg = tempRegIndex;
					tempRegIndex++;

					if (tempIsRet) {
						addRetStmnt(retType, ((object) expr).get_type().str,
								(tempRegIndex - 1));
					}
					return objReg;

				} else {
					int index = classToAttribCodeSnippets.get(
							currClass.getName()).get(((object) expr).name);

					cgen_stream.println("%" + tempRegIndex
							+ " = bitcast i8* %self_object" + " to %struct."
							+ currClass.getName().str + "*");
					tempRegIndex++;
					cgen_stream.println("%" + tempRegIndex
							+ " = getelementptr inbounds %struct."
							+ currClass.getName().str + "* %"
							+ (int) (tempRegIndex - 1) + ", i32 0, i32 "
							+ index);
					tempRegIndex++;

					cgen_stream.println("%" + tempRegIndex + " = load %struct."
							+ ((object) expr).get_type() + "** %"
							+ (int) (tempRegIndex - 1));
					int objReg = tempRegIndex;

					tempRegIndex++;

					if (tempIsRet) {
						addRetStmnt(retType, ((object) expr).get_type().str,
								(tempRegIndex - 1));
					}
					return objReg;
				}
			}

		} else if (expr instanceof string_const) {
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			String tempStr = ((string_const) expr).token.str;
			int token_index = ((string_const) expr).token.index;
			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.String* %"
					+ objRegister + ", i32 0, i32 3 ");
			tempRegIndex++;
			cgen_stream.println("store i8* getelementptr inbounds (["
					+ (int) (tempStr.length() + 1) + " x i8]* @.str"
					+ token_index + ", i32 0, i32 0),i8** %"
					+ (int) (tempRegIndex - 1));

			if (tempIsRet) {
				addRetStmnt(retType, "String", objRegister);
			}
			return objRegister;

		} else if (expr instanceof comp) { // not expr check
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((comp) expr).e1);

			int intValIndex = 3;
			int e1_register;
			int objRegister = genDynamicObjectCode("Bool");

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = xor i32 %"
					+ e1_register + ", 1");
			int resInd = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + objRegister
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resInd
					+ ", i32* %" + (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Bool", objRegister);
			}

			return objRegister;

		} else if (expr instanceof cond) { // if expr then expr else expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			ifelseBlkNum++;
			int currentBlk = ifelseBlkNum;
			int loopLastObjectIndex1, loopLastObjectIndex2, loopLastObjectIndex;
			String exprType = ((cond) expr).get_type().str;
			String thenExpType = ((cond) expr).then_exp.get_type().str;
			String elseExprType = ((cond) expr).else_exp.get_type().str;

			if (((cond) expr).get_type() == TreeConstants.SELF_TYPE) {
				exprType = currClass.getName().str;
			}

			if (((cond) expr).then_exp.get_type() == TreeConstants.SELF_TYPE) {
				thenExpType = currClass.getName().str;
			}

			if (((cond) expr).else_exp.get_type() == TreeConstants.SELF_TYPE) {
				elseExprType = currClass.getName().str;
			}
			cgen_stream.println("%" + tempRegIndex + " = alloca  %struct."
					+ exprType + "* ");
			loopLastObjectIndex = tempRegIndex;
			tempRegIndex++;

			int condResReg = visitExpr(currClass, ((cond) expr).pred);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool" + "* %"
					+ condResReg + ", i32 0, i32 3");
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1));
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = trunc i32 %"
					+ (int) (tempRegIndex - 1) + " to i1");
			tempRegIndex++;

			cgen_stream.println("br i1 %" + (int) (tempRegIndex - 1)
					+ ", label %.label" + currentBlk + ".t, label %.label"
					+ currentBlk + ".f");
			cgen_stream.println();

			cgen_stream.println(".label" + currentBlk + ".t: ;ifelseBlkNum: "
					+ ifelseBlkNum + " currentBlk: " + currentBlk);
			loopLastObjectIndex1 = visitExpr(currClass, ((cond) expr).then_exp);
			cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
					+ thenExpType + "* %" + loopLastObjectIndex1
					+ " to %struct." + exprType + "* ");

			loopLastObjectIndex1 = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("store %struct." + exprType + "* %"
					+ loopLastObjectIndex1 + ", %struct." + exprType + "** %"
					+ loopLastObjectIndex);

			cgen_stream.println("br label %.label" + currentBlk + ".elif");

			// tempRegIndex++;
			cgen_stream.println();

			cgen_stream.println(".label" + currentBlk + ".f: ;ifelseBlkNum: "
					+ ifelseBlkNum + " currentBlk: " + currentBlk);
			// cgen_stream.println("br label %"+tempRegIndex);

			loopLastObjectIndex2 = visitExpr(currClass, ((cond) expr).else_exp);
			cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
					+ elseExprType + "* %" + loopLastObjectIndex2
					+ " to %struct." + exprType + "* ");

			loopLastObjectIndex2 = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("store %struct." + exprType + "* %"
					+ loopLastObjectIndex2 + ", %struct." + exprType + "** %"
					+ loopLastObjectIndex);

			cgen_stream.println("br label %.label" + currentBlk + ".elif");
			cgen_stream.println(".label" + currentBlk + ".elif:");
			cgen_stream.println("%" + tempRegIndex + " = load %struct."
					+ exprType + "** %" + loopLastObjectIndex);
			loopLastObjectIndex = tempRegIndex;
			tempRegIndex++;

			cgen_stream.println();

			if (tempIsRet) {
				addRetStmnt(retType, exprType, loopLastObjectIndex);
			}
			return loopLastObjectIndex;

		} else if (expr instanceof divide) { // expr / expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((divide) expr).e1);
			int e2 = visitExpr(currClass, ((divide) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = sdiv nsw i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			// create temp Int object and store the result

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resReg + ", i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}

			return objRegister;

		} else if (expr instanceof plus) { // expr + expr
			cgen_stream.println(";" + expr.lineNumber + " plus");
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((plus) expr).e1);
			int e2 = visitExpr(currClass, ((plus) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = add nsw i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			// create temp Int object and store the result

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resReg + ", i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}

			return objRegister;

		} else if (expr instanceof sub) { // expr - expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((sub) expr).e1);
			int e2 = visitExpr(currClass, ((sub) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = sub nsw i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			// create temp Int object and store the result

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resReg + ", i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}

			return objRegister;

		} else if (expr instanceof mul) { // expr * expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((mul) expr).e1);
			int e2 = visitExpr(currClass, ((mul) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = mul nsw i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			// create temp Int object and store the result

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resReg + ", i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}

			return objRegister;

		} else if (expr instanceof neg) { // ~ expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((neg) expr).e1);

			int intValIndex = 3;
			int e1_register;
			int objRegister = genDynamicObjectCode("Int");

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = sub nsw i32 0, %"
					+ e1_register);
			int resInd = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("store i32 %" + resInd
					+ ", i32* %" + (int) (tempRegIndex - 1) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}

			return objRegister;


		} else if (expr instanceof int_const) { // integer constant
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;
			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("store i32 " + ((int_const) expr).token.str
					+ ", i32* %" + (int) (tempRegIndex - 1) + ", align 4");
			if (tempIsRet) {
				addRetStmnt(retType, "Int", objRegister);
			}
			return objRegister;

		} else if (expr instanceof leq) { // expr <= expr
			cgen_stream.println(";" + expr.lineNumber);

			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((leq) expr).e1);
			int e2 = visitExpr(currClass, ((leq) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = icmp sle i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = zext i1 %" + resReg
					+ " to i32");
			tempRegIndex++;

			cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
					+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Bool", objRegister);

			}

			return objRegister;

		} else if (expr instanceof lt) { // expr < expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((lt) expr).e1);
			int e2 = visitExpr(currClass, ((lt) expr).e2);

			int e1_register;
			int e2_register;

			int intValIndex = 3;

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e1
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e1_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Int* %" + e2
					+ ", i32 0, i32 " + intValIndex);
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1) + ", align 4");
			e2_register = tempRegIndex;
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = icmp slt i32 %"
					+ e1_register + ", %" + e2_register);
			int resReg = tempRegIndex;
			tempRegIndex++;

			int objRegister = genDynamicObjectCode(expr.get_type().str);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool* %" + objRegister
					+ ", i32 0, i32 3");
			tempRegIndex++;
			cgen_stream.println("%" + tempRegIndex + " = zext i1 %" + resReg
					+ " to i32");
			tempRegIndex++;

			cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
					+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

			if (tempIsRet) {
				addRetStmnt(retType, "Bool", objRegister);

			}

			return objRegister;

		} else if (expr instanceof eq) { // expr = expr
			cgen_stream.println(";" + expr.lineNumber);
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int e1 = visitExpr(currClass, ((eq) expr).e1);
			int e2 = visitExpr(currClass, ((eq) expr).e2);

			int e1_register;
			int e2_register;

			if (((eq) expr).e1.get_type() == TreeConstants.Int) {
				int intValIndex = 3;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Int* %" + e1
						+ ", i32 0, i32 " + intValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i32* %"
						+ (int) (tempRegIndex - 1) + ", align 4");
				e1_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Int* %" + e2
						+ ", i32 0, i32 " + intValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i32* %"
						+ (int) (tempRegIndex - 1) + ", align 4");
				e2_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = icmp eq i32 %"
						+ e1_register + ", %" + e2_register);
				int resReg = tempRegIndex;
				tempRegIndex++;

				int objRegister = genDynamicObjectCode(expr.get_type().str);

				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %"
						+ objRegister + ", i32 0, i32 3");
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = zext i1 %"
						+ resReg + " to i32");
				tempRegIndex++;

				cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
						+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

				if (tempIsRet) {
					addRetStmnt(retType, "Bool", objRegister);

				}

				return objRegister;

			} else if (((eq) expr).e1.get_type() == TreeConstants.Str) {

				int strValIndex = 3;
				int strCmpIndex;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.String* %" + e1
						+ ", i32 0, i32 " + strValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i8** %"
						+ (int) (tempRegIndex - 1) + ", align 8");
				e1_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.String* %" + e2
						+ ", i32 0, i32 " + strValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i8** %"
						+ (int) (tempRegIndex - 1) + ", align 8");
				e2_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex
						+ " = call i32 @strcmp(i8* %" + e1_register + ", i8* %"
						+ e2_register + ")");
				strCmpIndex = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = icmp eq i32 %"
						+ strCmpIndex + ", 0");
				int resReg = tempRegIndex;
				tempRegIndex++;

				int objRegister = genDynamicObjectCode(expr.get_type().str);

				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %"
						+ objRegister + ", i32 0, i32 3");
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = zext i1 %"
						+ resReg + " to i32");
				tempRegIndex++;

				cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
						+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

				if (tempIsRet) {
					addRetStmnt(retType, "Bool", objRegister);

				}

				return objRegister;

			} else if (((eq) expr).e1.get_type() == TreeConstants.Bool) {
				int boolValIndex = 3;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %" + e1
						+ ", i32 0, i32 " + boolValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i32* %"
						+ (int) (tempRegIndex - 1) + ", align 4");
				e1_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %" + e2
						+ ", i32 0, i32 " + boolValIndex);
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = load i32* %"
						+ (int) (tempRegIndex - 1) + ", align 4");
				e2_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = icmp eq i32 %"
						+ e1_register + ", %" + e2_register);
				int resReg = tempRegIndex;
				tempRegIndex++;

				int objRegister = genDynamicObjectCode(expr.get_type().str);

				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %"
						+ objRegister + ", i32 0, i32 3");
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = zext i1 %"
						+ resReg + " to i32");
				tempRegIndex++;

				cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
						+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

				if (tempIsRet) {
					addRetStmnt(retType, "Bool", objRegister);
				}

				return objRegister;

			} else {
				// TODO: check the allocated type from type field in object
				// TODO: if types are same then check for pointer or else return
				// false

				cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
						+ ((eq) expr).e1.get_type().str + "* %" + e1
						+ "to i32*");
				e1_register = tempRegIndex;
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
						+ ((eq) expr).e2.get_type().str + "* %" + e2
						+ "to i32*");
				e2_register = tempRegIndex;
				tempRegIndex++;

				cgen_stream.println("%" + tempRegIndex + " = icmp eq i32* %"
						+ e1_register + ", %" + e2_register);
				int resReg = tempRegIndex;
				tempRegIndex++;

				int objRegister = genDynamicObjectCode(expr.get_type().str);

				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct.Bool* %"
						+ objRegister + ", i32 0, i32 3");
				tempRegIndex++;
				cgen_stream.println("%" + tempRegIndex + " = zext i1 %"
						+ resReg + " to i32");
				tempRegIndex++;

				cgen_stream.println("store i32 %" + (int) (tempRegIndex - 1)
						+ ", i32* %" + (int) (tempRegIndex - 2) + ", align 4");

				if (tempIsRet) {
					addRetStmnt(retType, "Bool", objRegister);

				}
				return objRegister;
			}

		} else if (expr instanceof let) { // let expression check
			cgen_stream.println(";" + expr.lineNumber);

			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			let letExpr = (let) expr;
			Expression body = ((let) expr).body;
			Expression init = ((let) expr).init;

			if (!inLetExpr) {
				inLetExpr = true;
				letDepth = 0;
				letVarToIndex = new HashMap<AbstractSymbol, Integer>();
				letVarToType = new HashMap<AbstractSymbol, AbstractSymbol>();
				;
			}
			letDepth++;

			System.out.println("letDepth is :" + letDepth);
			// TODO : check for the type can have SELF_TYPE
			// TODO : add attribute to letVarToIndex map

			letVarToType.put(letExpr.identifier, letExpr.type_decl);

			if (init.get_type() != TreeConstants.No_type) {

				int objRegister = visitExpr(currClass, init);
				cgen_stream.println("%" + tempRegIndex + " = alloca %struct."
						+ letExpr.type_decl.str + "*, align 8");
				tempRegIndex++;
				letVarToIndex.put(letExpr.identifier, (tempRegIndex - 1));
				if (init.get_type() != letExpr.type_decl) {
					cgen_stream.println("%" + tempRegIndex
							+ " = bitcast %struct." + init.get_type().str
							+ "* %" + objRegister + " to %struct."
							+ letExpr.type_decl.str + "* ");
					tempRegIndex++;
					cgen_stream.println("store %struct."
							+ letExpr.type_decl.str + "* %"
							+ (int) (tempRegIndex - 1) + ", %struct."
							+ letExpr.type_decl.str + "** %"
							+ (int) (tempRegIndex - 2));
				} else {

					cgen_stream.println("store %struct."
							+ letExpr.type_decl.str + "* %" + objRegister
							+ ", %struct." + letExpr.type_decl.str + "** %"
							+ (int) (tempRegIndex - 1));
				}

			} else {
				cgen_stream.println("%" + tempRegIndex + " = alloca %struct."
						+ letExpr.type_decl.str + "*, align 8");
				tempRegIndex++;
				letVarToIndex.put(letExpr.identifier, (tempRegIndex - 1));
				if (letExpr.type_decl == TreeConstants.Int
						|| letExpr.type_decl == TreeConstants.Str
						|| letExpr.type_decl == TreeConstants.Bool) {
					int objRegister = genDynamicObjectCode(letExpr.type_decl.str);

					cgen_stream.println("store %struct."
							+ letExpr.type_decl.str + "* %" + objRegister
							+ ", %struct." + letExpr.type_decl.str + "** %"
							+ (int) (tempRegIndex - 2));

				} else {
					cgen_stream.println("%" + tempRegIndex
							+ " = alloca %struct." + letExpr.type_decl.str
							+ "*, align 8");
					tempRegIndex++;
					cgen_stream.println("store %struct."
							+ letExpr.type_decl.str + "* null, %struct."
							+ letExpr.type_decl.str + "** %"
							+ (int) (tempRegIndex - 1));

				}
			}
			isRetExpr = tempIsRet;
			int letBodyReg = visitExpr(currClass, body);
			System.out.println("last reg in let body is :"+letBodyReg);

			// inLetExpr = false;
			letDepth--;
			if (letDepth == 1 && inLetExpr) {
				System.out.println("turned to false");
				inLetExpr = false;
			}
			

			/*if (tempIsRet) {
				cgen_stream.println("ret %struct." + retType + "* null");
			}*/

			return letBodyReg;

		} else if (expr instanceof loop) { // loop expression check
			// TODO : store the boolean value and flip it back after checking
			// condition
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;
			cgen_stream.println(";" + expr.lineNumber);
			loopNum++;

			int loopCondLabel = loopNum;
			int predEvalLabel = tempRegIndex;
			cgen_stream.println("br label %" + predEvalLabel);
			cgen_stream.println("\n; <label>:" + predEvalLabel);
			tempRegIndex++;
			int condResReg = visitExpr(currClass, ((loop) expr).pred);

			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct.Bool" + "* %"
					+ condResReg + ", i32 0, i32 3");
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = load i32* %"
					+ (int) (tempRegIndex - 1));
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = trunc i32 %"
					+ (int) (tempRegIndex - 1) + " to i1");
			tempRegIndex++;

			cgen_stream.println("br i1 %" + (int) (tempRegIndex - 1)
					+ ", label %.label" + loopCondLabel
					+ ".loop.t, label %.label" + loopCondLabel + ".loop.f");
			cgen_stream.println();

			cgen_stream.println(".label" + loopCondLabel + ".loop.t:");
			int loopLastObjectIndex = visitExpr(currClass, ((loop) expr).body);

			cgen_stream.println("br label %" + predEvalLabel);
			// tempRegIndex++;
			cgen_stream.println();

			cgen_stream.println(".label" + loopCondLabel + ".loop.f:");

			if (tempIsRet) {
				cgen_stream.println("ret %struct." + retType + "* null");
			}
			return loopLastObjectIndex;

		} else if (expr instanceof typcase) { // switch case expression check
			cgen_stream.println(";" + expr.lineNumber);
			Cases swCases = ((typcase) expr).cases;
			Expression cond = ((typcase) expr).expr;
			// visitExpr(currClass, currClsSymbolTable, cond);

			HashSet<AbstractSymbol> swCasesSet = new HashSet<>();
			Enumeration cases = swCases.getElements();
			branch curBranch;
			AbstractSymbol exprType = null, tmpType;
			// If type of branch is undefined then do not add it to branch
			// for undefined typed do not check for duplications

			while (cases.hasMoreElements()) {
				curBranch = (branch) cases.nextElement();
				if (classTable.classListSet.contains(curBranch.type_decl)) {
					if (swCasesSet.contains(curBranch.type_decl)) {
						classTable.semantError(currClass.getFilename(), expr)
								.println("already contains this element");
					} else {
						swCasesSet.add(curBranch.type_decl);
					}
					// currClsSymbolTable.enterScope();
					// currClsSymbolTable.addId(curBranch.name,
					// curBranch.type_decl);
					// visitExpr(currClass, currClsSymbolTable, curBranch.expr);
					// currClsSymbolTable.exitScope();

					tmpType = curBranch.expr.get_type();

					if (exprType != null) {
						exprType = LCA(exprType, tmpType);
					} else {
						exprType = tmpType;
					}

				} else {
					classTable.semantError(currClass.getFilename(), expr)
							.println("Unknow type " + curBranch.type_decl.str);

					// currClsSymbolTable.enterScope();
					// currClsSymbolTable.addId(curBranch.name,
					// TreeConstants.Object_);
					// visitExpr(currClass, currClsSymbolTable, curBranch.expr);
					// currClsSymbolTable.exitScope();
				}
			}

			expr.set_type(exprType);

		} else if (expr instanceof assign) { // assign expression check id <-
												// expression
			cgen_stream.println(";" + expr.lineNumber);
			assign tempAssign = (assign) expr;
			boolean tempIsRet = isRetExpr;
			isRetExpr = false;

			int objRegister = visitExpr(currClass, tempAssign.expr);
			int tempObjReg;

			if (inLetExpr && letVarToIndex.containsKey(tempAssign.name)) {

				if (letVarToType.get(tempAssign.name) != tempAssign.expr
						.get_type()) {
					cgen_stream.println("%" + tempRegIndex
							+ " = bitcast %struct."
							+ tempAssign.expr.get_type().str + "* %"
							+ objRegister + " to %struct."
							+ letVarToType.get(tempAssign.name).str + "* ");
					tempRegIndex++;

					cgen_stream.println("store %struct."
							+ letVarToType.get(tempAssign.name).str + "* %"
							+ (int) (tempRegIndex - 1) + ", %struct."
							+ letVarToType.get(tempAssign.name).str + "** %"
							+ letVarToIndex.get(tempAssign.name));

					if (tempIsRet) {
						addRetStmnt(retType,
								letVarToType.get(tempAssign.name).str,
								objRegister);
					}
				} else {

					cgen_stream.println("store %struct."
							+ letVarToType.get(tempAssign.name).str + "* %"
							+ objRegister + ", %struct."
							+ letVarToType.get(tempAssign.name).str + "** %"
							+ letVarToIndex.get(tempAssign.name));

					if (tempIsRet) {
						addRetStmnt(retType,
								letVarToType.get(tempAssign.name).str,
								objRegister);

					}
				}

			} else if (methodFormalParams.containsKey(tempAssign.name)) {

				AbstractSymbol idType = methodFormalParams.get(tempAssign.name);

				if (idType != tempAssign.expr.get_type()) {

					cgen_stream.println("%" + tempRegIndex
							+ " = bitcast %struct."
							+ tempAssign.expr.get_type().str + "* %"
							+ objRegister + " to %struct." + idType.str + "* ");
					tempRegIndex++;

					cgen_stream.println("store %struct." + idType.str + " %"
							+ (int) (tempRegIndex - 1) + "*, %struct."
							+ idType.str + "** %"
							+ methodFormalParamsToIndex.get(tempAssign.name));

					if (tempIsRet) {
						addRetStmnt(retType, idType.str, objRegister);
					}
				} else {

					cgen_stream.println("store %struct." + idType.str + "* %"
							+ objRegister + ", %struct." + idType.str + "** %"
							+ methodFormalParamsToIndex.get(tempAssign.name));

					if (tempIsRet) {
						addRetStmnt(retType, tempAssign.expr.get_type().str,
								objRegister);
					}

				}
			} else {
				int index = classToAttribCodeSnippets.get(currClass.getName())
						.get(tempAssign.name);

				AbstractSymbol idType = attrCheck(currClass.getName(),
						tempAssign.name);

				cgen_stream.println("%" + tempRegIndex + " = bitcast i8* "
						+ "%self_object" + " to %struct."
						+ currClass.getName().str + "*");
				tempRegIndex++;

				cgen_stream.println("%" + tempRegIndex
						+ " = getelementptr inbounds %struct."
						+ currClass.getName().str + "* %"
						+ (int) (tempRegIndex - 1) + ", i32 0, i32 " + index);
				tempRegIndex++;

				if (idType != tempAssign.expr.get_type()) {

					cgen_stream.println("%" + tempRegIndex
							+ " = bitcast %struct."
							+ tempAssign.expr.get_type().str + "* %"
							+ objRegister + " to %struct." + idType.str + "* ");
					tempRegIndex++;

					cgen_stream.println("store %struct."
							+ tempAssign.expr.get_type().str + "* %"
							+ (int) (tempRegIndex - 1) + ", %struct."
							+ tempAssign.expr.get_type().str + "** %"
							+ (int) (tempRegIndex - 2));

					if (tempIsRet) {
						addRetStmnt(retType,
								letVarToType.get(tempAssign.name).str,
								objRegister);
					}
				} else {

					cgen_stream.println("store %struct." + idType.str + "* %"
							+ objRegister + ", %struct." + idType.str + "** %"
							+ (int) (tempRegIndex - 1));

					if (tempIsRet) {
						addRetStmnt(retType, tempAssign.expr.get_type().str,
								objRegister);
					}
				}
				return objRegister;
			}
			return objRegister;
		}
		return tempRegIndex;
	}

	/*
	 * 
	 * generate object code create object using proto type struct
	 */
	// TODO : fix size
	/*
	 * private int genObjectCode(String type) { /* 1. allocate memory in stack
	 * 2. bitcast (typecast) to i8* 3. load element pointer of size from Int to
	 * register 4. sign extend to 64 bits 5. memcpy prototype Int to stack
	 * allocated object
	 * 
	 * cgen_stream.println("%" + tempRegIndex + " = alloca %struct." + type +
	 * ", align 8"); tempRegIndex++; cgen_stream.println("%" + tempRegIndex +
	 * " = bitcast %struct." + type + "* %" + (int) (tempRegIndex - 1) +
	 * " to i8*"); tempRegIndex++; cgen_stream.println("%" + tempRegIndex +
	 * " = load i32* getelementptr inbounds (%struct." + type + "* @" + type +
	 * "_proto, i32 0, i32 2), align 4"); tempRegIndex++;
	 * cgen_stream.println("%" + tempRegIndex + " = sext i32 %" + (int)
	 * (tempRegIndex - 1) + " to i64"); tempRegIndex++; cgen_stream.println("%"
	 * + tempRegIndex + " = call i8* @__memcpy_chk(i8* %" + (int) (tempRegIndex
	 * - 3) + ", i8* bitcast (%struct." + type + "* @" + type +
	 * "_proto to i8*), i64 %" + (int) (tempRegIndex - 1) + ", i64 %" + (int)
	 * (tempRegIndex - 1) + " ) #2"); tempRegIndex++;
	 * 
	 * 
	 * 
	 * return tempRegIndex - 5; }
	 */

	private int genDynamicObjectCode(String type) {
		/*
		 * 1. load ptr to size of proto_type object 2. sign extend it to 64 bits
		 * 3. dynamically allocate size amount of bytes 4. copy the proto type
		 * object 2. bitcast (typecast) to i8* 3. load element pointer of size
		 * from Int to register 4. sign extend to 64 bits 5. memcpy prototype
		 * Int to stack allocated object
		 */

		cgen_stream.println("%" + tempRegIndex + " = call %struct." + type
				+ "* @." + type + "()");
		tempRegIndex++;

		return tempRegIndex - 1;
	}

	/*
	 * private void addRetStmnt(String typeRet, String objType, int index) {
	 * 
	 * cgen_stream.println("%" + tempRegIndex +
	 * " = load i32* getelementptr inbounds (%struct." + objType + "* %" + index
	 * + ", i32 0, i32 1), align 4"); tempRegIndex++;
	 * 
	 * cgen_stream.println("%" + tempRegIndex + " = sext i32 %" + (int)
	 * (tempRegIndex - 1) + " to i64"); tempRegIndex++;
	 * 
	 * cgen_stream.println("%" + tempRegIndex + " = call i8* @malloc(i64 " + "%"
	 * + (int) (tempRegIndex - 1) + ")"); tempRegIndex++;
	 * 
	 * cgen_stream.println("%" + tempRegIndex +
	 * " = call i8* @__memcpy_chk(i8* %" + (int) (tempRegIndex - 3) +
	 * ", i8* bitcast (%struct." + objType + "* %" + index + " to i8*), i64 %" +
	 * (int) (tempRegIndex - 1) + ", i64 %" + (int) (tempRegIndex - 1) +
	 * " ) #2"); tempRegIndex++;
	 * 
	 * cgen_stream.println("%" + tempRegIndex + " = bitcast (i8* %" + (int)
	 * (tempRegIndex - 1) + " to %struct." + typeRet + "* )"); tempRegIndex++;
	 * cgen_stream.println("ret %struct." + typeRet + "* %" + (int)
	 * (tempRegIndex - 1)); }
	 */

	private void addRetStmnt(String typeRet, String objType, int index) {
		cgen_stream.println("%" + tempRegIndex + " = bitcast %struct."
				+ objType + "* %" + index + " to %struct." + typeRet + "* ");
		tempRegIndex++;
		cgen_stream.println("ret %struct." + typeRet + "* %"
				+ (int) (tempRegIndex - 1));
	}

	private void genMain() {
		int regCounter = 0;
		cgen_stream.println("define i32 @main(){");
		cgen_stream.println("%1 = alloca i32, align 4");
		cgen_stream.println("store i32 0, i32* %1");

		addBasicInitsToMain();
		Set<AbstractSymbol> classesSet = classToMethodCodeSnippets.keySet();
		Iterator<AbstractSymbol> classItr = classesSet.iterator();
		while (classItr.hasNext()) {
			AbstractSymbol classSym = classItr.next();
			HashMap<AbstractSymbol, Integer> classToMethods = classToMethodCodeSnippets
					.get(classSym);
			Set<AbstractSymbol> allMethods = classToMethods.keySet();
			Iterator<AbstractSymbol> methodItr = allMethods.iterator();
			while (methodItr.hasNext()) {
				AbstractSymbol methodName = methodItr.next();
				int methodInd = classToMethods.get(methodName);
				AbstractSymbol methodClass = getClassEnvForMethod(methodName,
						classSym);

				ArrayList<AbstractSymbol> formals = methodEnv.get(methodClass)
						.get(methodName);

				ArrayList<Integer> regs = new ArrayList<Integer>();

				String dispatchReturnType = formals.get(formals.size() - 1).str;
				// store %struct.Object* (...)* bitcast (%struct.Object* (i8*)*
				// @Object_abort to %struct.Object* (...)*), %struct.Object*
				// (...)** getelementptr inbounds (%struct.Object_functions*
				// @Object_functions_proto, i32 0, i32 0), align 8

				if (formals.get(formals.size() - 1) == TreeConstants.SELF_TYPE) {
					dispatchReturnType = classSym.str;
					cgen_stream.print("store %struct." + dispatchReturnType
							+ "* (...)*  bitcast(%struct." + dispatchReturnType
							+ "* (i8*");
					for (int i = 0; i < formals.size() - 1; i++) {
						cgen_stream.print(", %struct." + formals.get(i).str
								+ "*");
					}
					cgen_stream.println(")* @" + classSym.str + "_"
							+ methodName.str + " to %struct."
							+ dispatchReturnType + "* (...)*), %struct."
							+ dispatchReturnType + "* (...)** "
							+ "getelementptr inbounds (%struct." + classSym.str
							+ "_functions* @" + classSym.str
							+ "_functions_proto, i32 0, i32 " + methodInd
							+ "), align 8");
				} else {
					cgen_stream.print("store %struct." + dispatchReturnType
							+ "* (...)*  bitcast(%struct." + dispatchReturnType
							+ "* (i8*");
					for (int i = 0; i < formals.size() - 1; i++) {
						cgen_stream.print(", %struct." + formals.get(i).str
								+ "*");
					}
					cgen_stream.println(")* @" + methodClass.str + "_"
							+ methodName.str + " to %struct."
							+ dispatchReturnType + "* (...)*), %struct."
							+ dispatchReturnType + "* (...)** "
							+ "getelementptr inbounds (%struct." + classSym.str
							+ "_functions* @" + classSym.str
							+ "_functions_proto, i32 0, i32 " + methodInd
							+ "), align 8");
				}
			}

		}
		Features featureSet = classTable.SymToClassMap.get(TreeConstants.Main).getFeatures();

		String mainRetType="Object";
		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof method ) {
				method tempMethod = (method) feature;
				String methodType;
				if(tempMethod.name == TreeConstants.main_meth){
					mainRetType=tempMethod.return_type.str;
					break;
				}

			}
		}
		cgen_stream.println("%2 = call  %struct.Main* @.Main()\n"
				+ "%3 = bitcast %struct.Main* %2 to i8*\n"
				+ "%4 = call %struct."+mainRetType+"* @Main_main (i8* %3) \n"
				+ "ret i32 0");
		cgen_stream.println("}");

	}

	private void genContructors() {
		Enumeration classElems = classes.getElements();
		while (classElems.hasMoreElements()) {
			tempRegIndex = 1;
			loopNum = 0;
			ifelseBlkNum = 0;
			isRetExpr = false;
			blockDepth = 0;
			letDepth = 0;
			inLetExpr = false;
			Class_ currCls = (Class_) classElems.nextElement();

			HashMap<AbstractSymbol, Integer> attribToSnippet = classToAttribCodeSnippets
					.get(currCls.getName());
			int numAttrs = attribToSnippet.size();
			cgen_stream.println("define %struct." + currCls.getName().str
					+ "* @." + currCls.getName().str + "(){");
			cgen_stream.println("%" + tempRegIndex + "= call i8* @malloc(i64 "
					+ (int) ((numAttrs + 3) * 8) + ")");
			tempRegIndex++;

			cgen_stream.println("%" + tempRegIndex + " = bitcast i8* " + "%"
					+ (int) (tempRegIndex - 1) + " to %struct."
					+ currCls.getName().str + "*");
			tempRegIndex++;
			generateConst(currCls.getName(), currCls.getName(), attribToSnippet);
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct."
					+ currCls.getName().str + "* %2 ,i32 0, i32 1");
			tempRegIndex++;
			cgen_stream.println("store i32 " + (int) ((numAttrs + 3) * 8)
					+ ", i32* %" + (int) (tempRegIndex - 1));
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct."
					+ currCls.getName().str + "* %2 ,i32 0, i32 0");
			tempRegIndex++;
			cgen_stream
					.println("store %struct.Object_functions* bitcast ( %struct."
							+ currCls.getName().str
							+ "_functions* @"
							+ currCls.getName().str
							+ "_functions_proto to %struct.Object_functions* ), %struct.Object_functions** %"
							+ (int) (tempRegIndex - 1));
			cgen_stream.println("%" + tempRegIndex
					+ " = getelementptr inbounds %struct."
					+ currCls.getName().str + "* %2 ,i32 0, i32 2");
			tempRegIndex++;
			cgen_stream.println("store i8* getelementptr inbounds (["
					+ (int) (currCls.getName().str.length() + 1) + " x i8]* @"
					+ currCls.getName().str + "_name, i32 0, i32 0), i8** %"
					+ (int) (tempRegIndex - 1));
			cgen_stream
					.println("ret %struct." + currCls.getName().str + "* %2");
			cgen_stream.println(" }");

		}
	}

	private int generateConst(AbstractSymbol currClass,
			AbstractSymbol parentClass,
			HashMap<AbstractSymbol, Integer> attribToSnippet) {
		int index;
		if (parentClass == TreeConstants.Object_) {
			index = 3;
		} else {
			index = generateConst(currClass,
					classTable.SymToClassMap.get(parentClass).getParent(),
					attribToSnippet);
		}

		Class_ currCls = classTable.SymToClassMap.get(parentClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof attr) {
				attr tempAttr = (attr) feature;
				String attribType;
				int AttrInd = attribToSnippet.get(tempAttr.name);
				int objReg;
				if (tempAttr.type_decl == TreeConstants.SELF_TYPE) {
					attribType = "%struct." + currClass.str + "*";
					attribToSnippet.get(tempAttr.name);
					if (tempAttr.init.get_type() == TreeConstants.No_type) {
						cgen_stream
								.println("%" + tempRegIndex
										+ " = getelementptr inbounds %struct."
										+ currClass.str + "* %2 ,i32 0, i32 "
										+ AttrInd);
						tempRegIndex++;
						cgen_stream.println("store %struct." + currClass.str
								+ "* null, %struct." + currClass.str + "** "
								+ (int) (tempRegIndex - 1));
					} else {
						objReg = visitExpr(
								classTable.SymToClassMap.get(currClass),
								tempAttr.init);
						cgen_stream
								.println("%" + tempRegIndex
										+ " = bitcast %struct."
										+ tempAttr.init.get_type().str + "* %"
										+ objReg + " to %struct."
										+ tempAttr.type_decl.str + "*");
						objReg = tempRegIndex;
						tempRegIndex++;
						cgen_stream
								.println("%" + tempRegIndex
										+ " = getelementptr inbounds %struct."
										+ currClass.str + "* %2 ,i32 0, i32 "
										+ AttrInd);
						tempRegIndex++;
						cgen_stream.println("store %struct."
								+ tempAttr.type_decl.str + "* %" + objReg
								+ ", %struct." + tempAttr.type_decl.str
								+ "** %" + (int) (tempRegIndex - 1));

					}

				} else {
					if (tempAttr.init.get_type() == TreeConstants.No_type) {
						if (tempAttr.type_decl == TreeConstants.Int
								|| tempAttr.type_decl == TreeConstants.Str
								|| tempAttr.type_decl == TreeConstants.Bool) {
							cgen_stream.println("%" + tempRegIndex
									+ " = call %struct."
									+ tempAttr.type_decl.str + "* @."
									+ tempAttr.type_decl.str + "()");
							objReg = tempRegIndex;
							tempRegIndex++;
							cgen_stream.println("%" + tempRegIndex
									+ " = getelementptr inbounds %struct."
									+ currClass.str + "* %2 ,i32 0, i32 "
									+ AttrInd);
							tempRegIndex++;
							cgen_stream.println("store %struct."
									+ tempAttr.type_decl.str + "* %" + objReg
									+ ", %struct." + tempAttr.type_decl.str
									+ "** %" + (int) (tempRegIndex - 1));

						} else {
							cgen_stream.println("%" + tempRegIndex
									+ " = getelementptr inbounds %struct."
									+ currClass.str + "* %2 ,i32 0, i32 "
									+ AttrInd);
							tempRegIndex++;
							cgen_stream.println("store %struct."
									+ tempAttr.type_decl.str
									+ "* null, %struct."
									+ tempAttr.type_decl.str + "** %"
									+ (int) (tempRegIndex - 1));
						}
					} else {
						objReg = visitExpr(
								classTable.SymToClassMap.get(currClass),
								tempAttr.init);
						cgen_stream
								.println("%" + tempRegIndex
										+ " = bitcast %struct."
										+ tempAttr.init.get_type().str + "* %"
										+ objReg + " to %struct."
										+ tempAttr.type_decl.str + "*");
						objReg = tempRegIndex;
						tempRegIndex++;
						cgen_stream
								.println("%" + tempRegIndex
										+ " = getelementptr inbounds %struct."
										+ currClass.str + "* %2 ,i32 0, i32 "
										+ AttrInd);
						tempRegIndex++;
						cgen_stream.println("store %struct."
								+ tempAttr.type_decl.str + "* %" + objReg
								+ ", %struct." + tempAttr.type_decl.str
								+ "** %" + (int) (tempRegIndex - 1));

					}
				}
			}
		}
		return index;
	}

	private void genSelfTypeFunctions() {
		Enumeration classElems = classes.getElements();
		while (classElems.hasMoreElements()) {
			Class_ currCls = (Class_) classElems.nextElement();

			generateSelfTypeMethod(currCls.getName(), currCls.getName());

		}

	}

	private int generateSelfTypeMethod(AbstractSymbol currClass,
			AbstractSymbol parentClass) {
		int index;
		if (parentClass == TreeConstants.Object_) {
			index = 0;
		} else {
			index = generateSelfTypeMethod(currClass, classTable.SymToClassMap
					.get(parentClass).getParent());
		}

		Class_ currCls = classTable.SymToClassMap.get(parentClass);

		Features featureSet = currCls.getFeatures();

		Enumeration features = featureSet.getElements();
		while (features.hasMoreElements()) {
			Feature feature = (Feature) features.nextElement();
			// check whether a feature is attribute or function definition

			if (feature instanceof method) {
				method tempMethod = (method) feature;
				String methodType;

				if (tempMethod.return_type == TreeConstants.SELF_TYPE) {
					AbstractSymbol methodClass = getClassEnvForMethod(
							tempMethod.name, currClass);

					if (methodClass != currClass) {
						ArrayList<AbstractSymbol> formals = methodEnv.get(
								methodClass).get(tempMethod.name);

						cgen_stream.print("define %struct." + currClass.str
								+ "* @" + currClass.str + "_"
								+ tempMethod.name.str + " (i8* %self_object");
						Formals formalParams = tempMethod.formals;
						Enumeration params = formalParams.getElements();
						while (params.hasMoreElements()) {
							formalc param = (formalc) params.nextElement();
							cgen_stream.print(", %struct."
									+ param.type_decl.str + "* %"
									+ param.name.str);

						}
						cgen_stream.println("){");
						/*
						 * 
						 * define %struct.IO* @IO_copy(i8* %self_obj) #0 { %1 =
						 * alloca i8*, align 8 store i8* %self_obj, i8** %1,
						 * align 8 %2 = load i8** %1, align 8 %3 = call
						 * %struct.Object* @Object_copy(i8* %2) %4 = bitcast
						 * %struct.Object* %3 to %struct.IO* ret %struct.IO* %4
						 * }
						 */
						cgen_stream.print("%1 = call %struct."
								+ currCls.getName().str + "* @"
								+ methodClass.str + "_" + tempMethod.name
								+ " (i8* %self_object");

						params = formalParams.getElements();
						while (params.hasMoreElements()) {
							formalc param = (formalc) params.nextElement();
							cgen_stream.print(", %struct."
									+ param.type_decl.str + "* %"
									+ param.name.str);

						}
						cgen_stream.println(")");

						cgen_stream.println("%2 = bitcast %struct."
								+ currCls.getName().str + "* %1 to %struct."
								+ currClass.str + "*");
						cgen_stream.println("ret %struct." + currClass.str
								+ "* %2");
						cgen_stream.println("}");

					}

				}

			}
			/*
			 * else { method tempMethod = (method) feature; if
			 * (tempMethod.return_type == TreeConstants.Int ||
			 * tempMethod.return_type == TreeConstants.Bool ||
			 * tempMethod.return_type == TreeConstants.Str) {
			 * 
			 * String methodSnippet = "%struct." + tempMethod.return_type +
			 * " (...)*"; methodToFuncProto.put(tempMethod.name, methodSnippet);
			 * } else { String methodSnippet = "%struct." +
			 * tempMethod.return_type + "* (...)*";
			 * methodToFuncProto.put(tempMethod.name, methodSnippet); }
			 * 
			 * }
			 */
		}
		return index;
	}

	private void addBasicInitsToMain() {

		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_abort to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.Object_functions* @Object_functions_proto, i32 0, i32 0), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @Object_type_name to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.Object_functions* @Object_functions_proto, i32 0, i32 1), align 8");
		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_copy to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.Object_functions* @Object_functions_proto, i32 0, i32 2), align 8");
		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_abort to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.Int_functions* @Int_functions_proto, i32 0, i32 0), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @Object_type_name to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.Int_functions* @Int_functions_proto, i32 0, i32 1), align 8");
		cgen_stream
				.println("store %struct.Int* (...)* bitcast (%struct.Int* (i8*)* @Int_copy to %struct.Int* (...)*), %struct.Int* (...)** getelementptr inbounds (%struct.Int_functions* @Int_functions_proto, i32 0, i32 2), align 8");
		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_abort to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.Bool_functions* @Bool_functions_proto, i32 0, i32 0), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @Object_type_name to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.Bool_functions* @Bool_functions_proto, i32 0, i32 1), align 8");
		cgen_stream
				.println("store %struct.Bool* (...)* bitcast (%struct.Bool* (i8*)* @Bool_copy to %struct.Bool* (...)*), %struct.Bool* (...)** getelementptr inbounds (%struct.Bool_functions* @Bool_functions_proto, i32 0, i32 2), align 8");
		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_abort to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 0), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @Object_type_name to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 1), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @String_copy to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 2), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*, %struct.Int*, %struct.Int*)* @String_substr to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 5), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*, %struct.String*)* @String_concat to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 4), align 8");
		cgen_stream
				.println("store %struct.Int* (...)* bitcast (%struct.Int* (i8*)* @String_length to %struct.Int* (...)*), %struct.Int* (...)** getelementptr inbounds (%struct.String_functions* @String_functions_proto, i32 0, i32 3), align 8");
		cgen_stream
				.println("store %struct.Object* (...)* bitcast (%struct.Object* (i8*)* @Object_abort to %struct.Object* (...)*), %struct.Object* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 0), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @Object_type_name to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 1), align 8");
		cgen_stream
				.println("store %struct.IO* (...)* bitcast (%struct.IO* (i8*)* @IO_copy to %struct.IO* (...)*), %struct.IO* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 2), align 8");
		cgen_stream
				.println("store %struct.Int* (...)* bitcast (%struct.Int* (i8*)* @IO_in_int to %struct.Int* (...)*), %struct.Int* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 6), align 8");
		cgen_stream
				.println("store %struct.String* (...)* bitcast (%struct.String* (i8*)* @IO_in_string to %struct.String* (...)*), %struct.String* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 5), align 8");
		cgen_stream
				.println("store %struct.IO* (...)* bitcast (%struct.IO* (i8*, %struct.Int*)* @IO_out_int to %struct.IO* (...)*), %struct.IO* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 4), align 8");
		cgen_stream
				.println("store %struct.IO* (...)* bitcast (%struct.IO* (i8*, %struct.String*)* @IO_out_string to %struct.IO* (...)*), %struct.IO* (...)** getelementptr inbounds (%struct.IO_functions* @IO_functions_proto, i32 0, i32 3), align 8");
	}

	private void addAllBasicFuncs() throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader("append.ll"));
		try {
			String line = br.readLine();

			while (line != null) {
				cgen_stream.println(line);
				line = br.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/**
 * Defines AST constructor 'class_c'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class class_c extends Class_ {
	protected AbstractSymbol name;
	protected AbstractSymbol parent;
	protected Features features;
	protected AbstractSymbol filename;

	/**
	 * Creates "class_c" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for parent
	 * @param a2
	 *            initial value for features
	 * @param a3
	 *            initial value for filename
	 */
	public class_c(int lineNumber, AbstractSymbol a1, AbstractSymbol a2,
			Features a3, AbstractSymbol a4) {
		super(lineNumber);
		name = a1;
		parent = a2;
		features = a3;
		filename = a4;
	}

	public TreeNode copy() {
		return new class_c(lineNumber, copy_AbstractSymbol(name),
				copy_AbstractSymbol(parent), (Features) features.copy(),
				copy_AbstractSymbol(filename));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "class_c\n");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, parent);
		features.dump(out, n + 2);
		dump_AbstractSymbol(out, n + 2, filename);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_class");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, parent);
		out.print(Utilities.pad(n + 2) + "\"");
		Utilities.printEscapedString(out, filename.getString());
		out.println("\"\n" + Utilities.pad(n + 2) + "(");
		for (Enumeration e = features.getElements(); e.hasMoreElements();) {
			((Feature) e.nextElement()).dump_with_types(out, n + 2);
		}
		out.println(Utilities.pad(n + 2) + ")");
	}

	public AbstractSymbol getName() {
		return name;
	}

	public AbstractSymbol getParent() {
		return parent;
	}

	public AbstractSymbol getFilename() {
		return filename;
	}

	public Features getFeatures() {
		return features;
	}

}

/**
 * Defines AST constructor 'method'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class method extends Feature {
	protected AbstractSymbol name;
	protected Formals formals;
	protected AbstractSymbol return_type;
	protected Expression expr;

	/**
	 * Creates "method" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for formals
	 * @param a2
	 *            initial value for return_type
	 * @param a3
	 *            initial value for expr
	 */
	public method(int lineNumber, AbstractSymbol a1, Formals a2,
			AbstractSymbol a3, Expression a4) {
		super(lineNumber);
		name = a1;
		formals = a2;
		return_type = a3;
		expr = a4;
	}

	public TreeNode copy() {
		return new method(lineNumber, copy_AbstractSymbol(name),
				(Formals) formals.copy(), copy_AbstractSymbol(return_type),
				(Expression) expr.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "method\n");
		dump_AbstractSymbol(out, n + 2, name);
		formals.dump(out, n + 2);
		dump_AbstractSymbol(out, n + 2, return_type);
		expr.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_method");
		dump_AbstractSymbol(out, n + 2, name);
		for (Enumeration e = formals.getElements(); e.hasMoreElements();) {
			((Formal) e.nextElement()).dump_with_types(out, n + 2);
		}
		dump_AbstractSymbol(out, n + 2, return_type);
		expr.dump_with_types(out, n + 2);
	}

}

/**
 * Defines AST constructor 'attr'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class attr extends Feature {
	protected AbstractSymbol name;
	protected AbstractSymbol type_decl;
	protected Expression init;

	/**
	 * Creates "attr" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for type_decl
	 * @param a2
	 *            initial value for init
	 */
	public attr(int lineNumber, AbstractSymbol a1, AbstractSymbol a2,
			Expression a3) {
		super(lineNumber);
		name = a1;
		type_decl = a2;
		init = a3;
	}

	public TreeNode copy() {
		return new attr(lineNumber, copy_AbstractSymbol(name),
				copy_AbstractSymbol(type_decl), (Expression) init.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "attr\n");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
		init.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_attr");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
		init.dump_with_types(out, n + 2);
	}

}

/**
 * Defines AST constructor 'formalc'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class formalc extends Formal {
	protected AbstractSymbol name;
	protected AbstractSymbol type_decl;

	/**
	 * Creates "formalc" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for type_decl
	 */
	public formalc(int lineNumber, AbstractSymbol a1, AbstractSymbol a2) {
		super(lineNumber);
		name = a1;
		type_decl = a2;
	}

	public TreeNode copy() {
		return new formalc(lineNumber, copy_AbstractSymbol(name),
				copy_AbstractSymbol(type_decl));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "formalc\n");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_formal");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
	}

}

/**
 * Defines AST constructor 'branch'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class branch extends Case {
	protected AbstractSymbol name;
	protected AbstractSymbol type_decl;
	protected Expression expr;

	/**
	 * Creates "branch" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for type_decl
	 * @param a2
	 *            initial value for expr
	 */
	public branch(int lineNumber, AbstractSymbol a1, AbstractSymbol a2,
			Expression a3) {
		super(lineNumber);
		name = a1;
		type_decl = a2;
		expr = a3;
	}

	public TreeNode copy() {
		return new branch(lineNumber, copy_AbstractSymbol(name),
				copy_AbstractSymbol(type_decl), (Expression) expr.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "branch\n");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
		expr.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_branch");
		dump_AbstractSymbol(out, n + 2, name);
		dump_AbstractSymbol(out, n + 2, type_decl);
		expr.dump_with_types(out, n + 2);
	}

}

/**
 * Defines AST constructor 'assign'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class assign extends Expression {
	protected AbstractSymbol name;
	protected Expression expr;

	/**
	 * Creates "assign" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 * @param a1
	 *            initial value for expr
	 */
	public assign(int lineNumber, AbstractSymbol a1, Expression a2) {
		super(lineNumber);
		name = a1;
		expr = a2;
	}

	public TreeNode copy() {
		return new assign(lineNumber, copy_AbstractSymbol(name),
				(Expression) expr.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "assign\n");
		dump_AbstractSymbol(out, n + 2, name);
		expr.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_assign");
		dump_AbstractSymbol(out, n + 2, name);
		expr.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'static_dispatch'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class static_dispatch extends Expression {
	protected Expression expr;
	protected AbstractSymbol type_name;
	protected AbstractSymbol name;
	protected Expressions actual;

	/**
	 * Creates "static_dispatch" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for expr
	 * @param a1
	 *            initial value for type_name
	 * @param a2
	 *            initial value for name
	 * @param a3
	 *            initial value for actual
	 */
	public static_dispatch(int lineNumber, Expression a1, AbstractSymbol a2,
			AbstractSymbol a3, Expressions a4) {
		super(lineNumber);
		expr = a1;
		type_name = a2;
		name = a3;
		actual = a4;
	}

	public TreeNode copy() {
		return new static_dispatch(lineNumber, (Expression) expr.copy(),
				copy_AbstractSymbol(type_name), copy_AbstractSymbol(name),
				(Expressions) actual.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "static_dispatch\n");
		expr.dump(out, n + 2);
		dump_AbstractSymbol(out, n + 2, type_name);
		dump_AbstractSymbol(out, n + 2, name);
		actual.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_static_dispatch");
		expr.dump_with_types(out, n + 2);
		dump_AbstractSymbol(out, n + 2, type_name);
		dump_AbstractSymbol(out, n + 2, name);
		out.println(Utilities.pad(n + 2) + "(");
		for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
			((Expression) e.nextElement()).dump_with_types(out, n + 2);
		}
		out.println(Utilities.pad(n + 2) + ")");
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'dispatch'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class dispatch extends Expression {
	protected Expression expr;
	protected AbstractSymbol name;
	protected Expressions actual;

	/**
	 * Creates "dispatch" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for expr
	 * @param a1
	 *            initial value for name
	 * @param a2
	 *            initial value for actual
	 */
	public dispatch(int lineNumber, Expression a1, AbstractSymbol a2,
			Expressions a3) {
		super(lineNumber);
		expr = a1;
		name = a2;
		actual = a3;
	}

	public TreeNode copy() {
		return new dispatch(lineNumber, (Expression) expr.copy(),
				copy_AbstractSymbol(name), (Expressions) actual.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "dispatch\n");
		expr.dump(out, n + 2);
		dump_AbstractSymbol(out, n + 2, name);
		actual.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_dispatch");
		expr.dump_with_types(out, n + 2);
		dump_AbstractSymbol(out, n + 2, name);
		out.println(Utilities.pad(n + 2) + "(");
		for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
			((Expression) e.nextElement()).dump_with_types(out, n + 2);
		}
		out.println(Utilities.pad(n + 2) + ")");
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'cond'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class cond extends Expression {
	protected Expression pred;
	protected Expression then_exp;
	protected Expression else_exp;

	/**
	 * Creates "cond" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for pred
	 * @param a1
	 *            initial value for then_exp
	 * @param a2
	 *            initial value for else_exp
	 */
	public cond(int lineNumber, Expression a1, Expression a2, Expression a3) {
		super(lineNumber);
		pred = a1;
		then_exp = a2;
		else_exp = a3;
	}

	public TreeNode copy() {
		return new cond(lineNumber, (Expression) pred.copy(),
				(Expression) then_exp.copy(), (Expression) else_exp.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "cond\n");
		pred.dump(out, n + 2);
		then_exp.dump(out, n + 2);
		else_exp.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_cond");
		pred.dump_with_types(out, n + 2);
		then_exp.dump_with_types(out, n + 2);
		else_exp.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'loop'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class loop extends Expression {
	protected Expression pred;
	protected Expression body;

	/**
	 * Creates "loop" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for pred
	 * @param a1
	 *            initial value for body
	 */
	public loop(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		pred = a1;
		body = a2;
	}

	public TreeNode copy() {
		return new loop(lineNumber, (Expression) pred.copy(),
				(Expression) body.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "loop\n");
		pred.dump(out, n + 2);
		body.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_loop");
		pred.dump_with_types(out, n + 2);
		body.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'typcase'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class typcase extends Expression {
	protected Expression expr;
	protected Cases cases;

	/**
	 * Creates "typcase" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for expr
	 * @param a1
	 *            initial value for cases
	 */
	public typcase(int lineNumber, Expression a1, Cases a2) {
		super(lineNumber);
		expr = a1;
		cases = a2;
	}

	public TreeNode copy() {
		return new typcase(lineNumber, (Expression) expr.copy(),
				(Cases) cases.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "typcase\n");
		expr.dump(out, n + 2);
		cases.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_typcase");
		expr.dump_with_types(out, n + 2);
		for (Enumeration e = cases.getElements(); e.hasMoreElements();) {
			((Case) e.nextElement()).dump_with_types(out, n + 2);
		}
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'block'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class block extends Expression {
	protected Expressions body;

	/**
	 * Creates "block" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for body
	 */
	public block(int lineNumber, Expressions a1) {
		super(lineNumber);
		body = a1;
	}

	public TreeNode copy() {
		return new block(lineNumber, (Expressions) body.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "block\n");
		body.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_block");
		for (Enumeration e = body.getElements(); e.hasMoreElements();) {
			((Expression) e.nextElement()).dump_with_types(out, n + 2);
		}
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'let'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class let extends Expression {
	protected AbstractSymbol identifier;
	protected AbstractSymbol type_decl;
	protected Expression init;
	protected Expression body;

	/**
	 * Creates "let" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for identifier
	 * @param a1
	 *            initial value for type_decl
	 * @param a2
	 *            initial value for init
	 * @param a3
	 *            initial value for body
	 */
	public let(int lineNumber, AbstractSymbol a1, AbstractSymbol a2,
			Expression a3, Expression a4) {
		super(lineNumber);
		identifier = a1;
		type_decl = a2;
		init = a3;
		body = a4;
	}

	public TreeNode copy() {
		return new let(lineNumber, copy_AbstractSymbol(identifier),
				copy_AbstractSymbol(type_decl), (Expression) init.copy(),
				(Expression) body.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "let\n");
		dump_AbstractSymbol(out, n + 2, identifier);
		dump_AbstractSymbol(out, n + 2, type_decl);
		init.dump(out, n + 2);
		body.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_let");
		dump_AbstractSymbol(out, n + 2, identifier);
		dump_AbstractSymbol(out, n + 2, type_decl);
		init.dump_with_types(out, n + 2);
		body.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'plus'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class plus extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "plus" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public plus(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new plus(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "plus\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_plus");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'sub'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class sub extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "sub" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public sub(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new sub(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "sub\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_sub");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'mul'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class mul extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "mul" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public mul(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new mul(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "mul\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_mul");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'divide'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class divide extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "divide" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public divide(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new divide(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "divide\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_divide");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'neg'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class neg extends Expression {
	protected Expression e1;

	/**
	 * Creates "neg" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 */
	public neg(int lineNumber, Expression a1) {
		super(lineNumber);
		e1 = a1;
	}

	public TreeNode copy() {
		return new neg(lineNumber, (Expression) e1.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "neg\n");
		e1.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_neg");
		e1.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'lt'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class lt extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "lt" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public lt(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new lt(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "lt\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_lt");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'eq'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class eq extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "eq" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public eq(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new eq(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "eq\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_eq");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'leq'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class leq extends Expression {
	protected Expression e1;
	protected Expression e2;

	/**
	 * Creates "leq" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 * @param a1
	 *            initial value for e2
	 */
	public leq(int lineNumber, Expression a1, Expression a2) {
		super(lineNumber);
		e1 = a1;
		e2 = a2;
	}

	public TreeNode copy() {
		return new leq(lineNumber, (Expression) e1.copy(),
				(Expression) e2.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "leq\n");
		e1.dump(out, n + 2);
		e2.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_leq");
		e1.dump_with_types(out, n + 2);
		e2.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'comp'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class comp extends Expression {
	protected Expression e1;

	/**
	 * Creates "comp" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 */
	public comp(int lineNumber, Expression a1) {
		super(lineNumber);
		e1 = a1;
	}

	public TreeNode copy() {
		return new comp(lineNumber, (Expression) e1.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "comp\n");
		e1.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_comp");
		e1.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'int_const'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class int_const extends Expression {
	protected AbstractSymbol token;

	/**
	 * Creates "int_const" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for token
	 */
	public int_const(int lineNumber, AbstractSymbol a1) {
		super(lineNumber);
		token = a1;
	}

	public TreeNode copy() {
		return new int_const(lineNumber, copy_AbstractSymbol(token));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "int_const\n");
		dump_AbstractSymbol(out, n + 2, token);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_int");
		dump_AbstractSymbol(out, n + 2, token);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method method is provided to you
	 * as an example of code generation.
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
		CgenSupport
				.emitLoadInt(CgenSupport.ACC,
						(IntSymbol) AbstractTable.inttable.lookup(token
								.getString()), s);
	}

}

/**
 * Defines AST constructor 'bool_const'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class bool_const extends Expression {
	protected Boolean val;

	/**
	 * Creates "bool_const" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for val
	 */
	public bool_const(int lineNumber, Boolean a1) {
		super(lineNumber);
		val = a1;
	}

	public TreeNode copy() {
		return new bool_const(lineNumber, copy_Boolean(val));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "bool_const\n");
		dump_Boolean(out, n + 2, val);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_bool");
		dump_Boolean(out, n + 2, val);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method method is provided to you
	 * as an example of code generation.
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
		CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(val), s);
	}

}

/**
 * Defines AST constructor 'string_const'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class string_const extends Expression {
	protected AbstractSymbol token;

	/**
	 * Creates "string_const" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for token
	 */
	public string_const(int lineNumber, AbstractSymbol a1) {
		super(lineNumber);
		token = a1;
	}

	public TreeNode copy() {
		return new string_const(lineNumber, copy_AbstractSymbol(token));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "string_const\n");
		dump_AbstractSymbol(out, n + 2, token);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_string");
		out.print(Utilities.pad(n + 2) + "\"");
		Utilities.printEscapedString(out, token.getString());
		out.println("\"");
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method method is provided to you
	 * as an example of code generation.
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
		CgenSupport.emitLoadString(CgenSupport.ACC,
				(StringSymbol) AbstractTable.stringtable.lookup(token
						.getString()), s);
	}

}

/**
 * Defines AST constructor 'new_'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class new_ extends Expression {
	protected AbstractSymbol type_name;

	/**
	 * Creates "new_" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for type_name
	 */
	public new_(int lineNumber, AbstractSymbol a1) {
		super(lineNumber);
		type_name = a1;
	}

	public TreeNode copy() {
		return new new_(lineNumber, copy_AbstractSymbol(type_name));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "new_\n");
		dump_AbstractSymbol(out, n + 2, type_name);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_new");
		dump_AbstractSymbol(out, n + 2, type_name);
		dump_type(out, n);
	}

	public AbstractSymbol get_type_name() {
		return type_name;
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'isvoid'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class isvoid extends Expression {
	protected Expression e1;

	/**
	 * Creates "isvoid" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for e1
	 */
	public isvoid(int lineNumber, Expression a1) {
		super(lineNumber);
		e1 = a1;
	}

	public TreeNode copy() {
		return new isvoid(lineNumber, (Expression) e1.copy());
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "isvoid\n");
		e1.dump(out, n + 2);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_isvoid");
		e1.dump_with_types(out, n + 2);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'no_expr'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class no_expr extends Expression {
	/**
	 * Creates "no_expr" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 */
	public no_expr(int lineNumber) {
		super(lineNumber);
	}

	public TreeNode copy() {
		return new no_expr(lineNumber);
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "no_expr\n");
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_no_expr");
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}

/**
 * Defines AST constructor 'object'.
 * <p>
 * See <a href="TreeNode.html">TreeNode</a> for full documentation.
 */
class object extends Expression {
	protected AbstractSymbol name;

	/**
	 * Creates "object" AST node.
	 * 
	 * @param lineNumber
	 *            the line in the source file from which this node came.
	 * @param a0
	 *            initial value for name
	 */
	public object(int lineNumber, AbstractSymbol a1) {
		super(lineNumber);
		name = a1;
	}

	public AbstractSymbol get_name() {
		return name;
	}

	public TreeNode copy() {
		return new object(lineNumber, copy_AbstractSymbol(name));
	}

	public void dump(PrintStream out, int n) {
		out.print(Utilities.pad(n) + "object\n");
		dump_AbstractSymbol(out, n + 2, name);
	}

	public void dump_with_types(PrintStream out, int n) {
		dump_line(out, n);
		out.println(Utilities.pad(n) + "_object");
		dump_AbstractSymbol(out, n + 2, name);
		dump_type(out, n);
	}

	/**
	 * Generates code for this expression. This method is to be completed in
	 * programming assignment 5. (You may add or remove parameters as you wish.)
	 * 
	 * @param s
	 *            the output stream
	 * */
	public void code(PrintStream s) {
	}

}
