package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRErrorStrategy;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;


public class Parser1 {
	public static String inputFile = null;
	public static Cool parser;
	public static ErrorListener errListener = null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		args = Flags.handleFlags(args);
		InputStream is = null;
		ANTLRInputStream input = null;
		
		
		for (int i = 0; i < args.length; i++) {
			inputFile = args[i];
			try {
				is = new FileInputStream(inputFile);
				input = new ANTLRInputStream(is);
				CoolLexerRules lexer = new CoolLexerRules(input);
				
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				
				parser = new Cool(tokens);
				parser.removeErrorListeners();
				parser.addErrorListener(new ErrorListener());
				ParseTree tree = parser.program(); // parse
				if(tree.getChild(0).getText().length()==0){
					System.err.println("\""+inputFile+"\"" + ", line 0: syntax error at or near EOF");
					System.err.println("Compilation halted due to lex and parse errors");
				}
				else{
					ASTBuilder eval = new ASTBuilder(inputFile);
					eval.visit(tree);
					if (eval.prgm != null
							&& ASTBuilder.encounteredError != true){
						
						eval.prgm.semant();
						//eval.prgm.dump_with_types(System.out, 0);
						FileOutputStream f = new FileOutputStream(inputFile+".ll");
						PrintStream outPut = new PrintStream(f);
						eval.prgm.cgen(outPut);
						
					}
					else
						System.err
								.println("Compilation halted due to lex and parse errors");
					//eval.prgm.semant();
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
