#!/bin/bash
file=./cases
if [ -d "out" ] ; then
  rm -rf out
fi
mkdir out
export CLASSPATH=$CLASSPATH:../src/java
while read line 
do
	echo $line
	java main.Parser1 $line > "out/$line.out" 2>&1
done < $file
